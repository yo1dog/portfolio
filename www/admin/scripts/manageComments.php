<?php
if (!isset($_POST['id']))
{
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
	return;
}

$id = $_POST['id'];

if (isset($_POST['delete']))
{
	$query = 'DELETE FROM project_comments WHERE id = \'' . mysql_real_escape_string($id) . '\'';
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	die('1');
}

if (isset($_POST['block']))
{
	if (!isset($_POST['ip']))
	{
		throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
		return;
	}
	
	$ip  = '\'' . mysql_real_escape_string($_POST['ip']) . '\'';
	
	$query = 'INSERT INTO project_comments_blocked_ips (ip) VALUES (' . $ip . ')';
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	$query = 'DELETE FROM project_comments WHERE ip = ' . $ip;
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	die('1');
}


if (!isset($_POST['name']) || !isset($_POST['text']) || !isset($_POST['approved']))
{
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
	return;
}

$id = '\'' . mysql_real_escape_string($id) . '\'';
$name = '\'' . mysql_real_escape_string(urldecode($_POST['name'])) . '\'';
$text = '\'' . mysql_real_escape_string(urldecode($_POST['text'])) . '\'';

$approved = $_POST['approved'];
if ($approved === 'null')
	$approved = 'NULL';
else
	$approved = '\'' . mysql_real_escape_string($approved) . '\'';

$query = "UPDATE project_comments SET name = $name, text = $text, approved = $approved WHERE id = $id";
$result = mysql_query($query);

if ($result === false)
	throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);

die('1');
?>