<?php
if (!isset($_POST['t']) || !isset($_POST['col0']))
{
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
	return;
}

$table = $_POST['t'];
$tableName = GetTableName($table);
$id = $_POST['col0'];

if (isset($_POST['delete']))
{
	$query = 'DELETE FROM ' . $tableName . ' WHERE id = \'' . mysql_real_escape_string($id) . '\'';
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	die('1');
}


$tableCols = GetTableColumns($table);
$numTableCols = count($tableCols) + 1;

for ($i = 1; $i < $numTableCols; $i++)
{
	if (!isset($_POST['col' . $i]))
	{
		throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
		return;
	}
}

if ($id == 'New')
{
	$query = 'INSERT INTO ' . $tableName . ' (';
	
	$first = true;
	foreach ($tableCols as $colName => $col)
	{
		if ($first)
			$first = false;
		else
			$query .= ', ';
		
		$query .=  $colName;
	}
	
	$query .= ') VALUES (';
	
	for ($i = 1; $i < $numTableCols; $i++)
	{
		if ($i > 1)
			$query .= ', ';
		
		$val = $_POST['col' . $i];
		if (strlen($val) === 0)
			$val = 'NULL';
		else
			$val = '\'' . mysql_real_escape_string(urldecode($val)) . '\'';
			
		$query .= $val;
	}
	
	$query .= ')';
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	die('1');
}
else
{
	$query = 'UPDATE ' . $tableName . ' SET ';
	
	$colIndex = 1;
	foreach ($tableCols as $colName => $col)
	{
		if ($colIndex > 1)
			$query .= ', ';
		
		$val = $_POST['col' . $colIndex];
		if (strlen($val) === 0)
			$val = 'NULL';
		else
			$val = '\'' . mysql_real_escape_string(urldecode($val)) . '\'';
		
		$query .= $colName . ' = ' . $val;
		
		$colIndex ++;
	}
	
	$query .= ' WHERE id = \'' . mysql_real_escape_string($id) . '\'';
	
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	die('1');
}
?>