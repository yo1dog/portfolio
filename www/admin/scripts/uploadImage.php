<?php
$result = 'false';
$exception = NULL;

try
{
	if (!isset($_FILES['imageFile']))
		throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
	
	$filename = '../images/projects/_temp' . IMAGE_EXT;
	
	if (file_exists($filename))
		unlink($filename);
	
	if (move_uploaded_file($_FILES['imageFile']['tmp_name'], $filename))
		$result = 'true';
}
catch (Exception $e)
{
	$exception = $e;
}
?>

<script type="text/javascript" language="javascript">
parent.uploadComplete(<?php echo($result); ?>);
</script>

<?php
if ($exception !== NULL)
	throw $exception;
?>