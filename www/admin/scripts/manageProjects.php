<?php
if (!isset($_POST['id']))
{
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
	return;
}

$id = $_POST['id'];

if (isset($_POST['delete']))
{
	$query = 'DELETE FROM projects WHERE id = \'' . mysql_real_escape_string($id) . '\'';
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	die('1');
}

if (!isset($_POST['name']) ||
	!isset($_POST['year1']) ||
	!isset($_POST['year2']) ||
	!isset($_POST['categoryID']) ||
	!isset($_POST['employerID']) ||
	!isset($_POST['description']) ||
	!isset($_POST['linkName']) ||
	!isset($_POST['linkURL']) ||
	!isset($_POST['imageChanged']) ||
	!isset($_POST['imageEmpty']) ||
	!isset($_POST['languageIDs']) ||
	!isset($_POST['frameworkIDs']) ||
	!isset($_POST['softwareIDs']))
{
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);
	return;
}

$name = '\'' . mysql_real_escape_string(urldecode($_POST['name'])) . '\'';
$year1 = '\'' . mysql_real_escape_string(urldecode($_POST['year1'])) . '\'';
$description = '\'' . mysql_real_escape_string(urldecode($_POST['description'])) . '\'';

$year2 = $_POST['year2'];
if (strlen($year2) === 0)
	$year2 = 'NULL';
else
	$year2 = '\'' . mysql_real_escape_string(urldecode($year2)) . '\'';

$categoryID = $_POST['categoryID'];
if (strlen($categoryID) === 0)
	$categoryID = 'NULL';
else
	$categoryID = '\'' . mysql_real_escape_string(urldecode($categoryID)) . '\'';

$employerID = $_POST['employerID'];
if (strlen($employerID) === 0)
	$employerID = 'NULL';
else
	$employerID = '\'' . mysql_real_escape_string(urldecode($employerID)) . '\'';

$linkName = $_POST['linkName'];
if (strlen($linkName) === 0)
	$linkName = 'NULL';
else
	$linkName = '\'' . mysql_real_escape_string(urldecode($linkName)) . '\'';

$linkURL = $_POST['linkURL'];
if (strlen($linkURL) === 0)
	$linkURL = 'NULL';
else
	$linkURL = '\'' . mysql_real_escape_string(urldecode($linkURL)) . '\'';

$imageChanged = $_POST['imageChanged'] === '1';
$imageEmpty = $_POST['imageEmpty'] === '1';

$languageIDs = array();
$numLanguageIDs = 0;
$languageStr = $_POST['languageIDs'];
$i = 0;
$l = strlen($languageStr);
while ($i < $l)
{
	$index = strpos($languageStr, ',', $i);
	$languageIDs[$numLanguageIDs] = substr($languageStr, $i, $index - $i);
	$numLanguageIDs ++;
	$i = $index + 1;
}

$frameworkIDs = array();
$numFrameworkIDs = 0;
$frameworkStr = $_POST['frameworkIDs'];
$i = 0;
$l = strlen($frameworkStr);
while ($i < $l)
{
	$index = strpos($frameworkStr, ',', $i);
	$frameworkIDs[$numFrameworkIDs] = substr($frameworkStr, $i, $index - $i);
	$numFrameworkIDs ++;
	$i = $index + 1;
}

$softwareIDs = array();
$numSoftwareIDs = 0;
$softwareStr = $_POST['softwareIDs'];
$i = 0;
$l = strlen($softwareStr);
while ($i < $l)
{
	$index = strpos($softwareStr, ',', $i);
	$softwareIDs[$numSoftwareIDs] = substr($softwareStr, $i, $index - $i);
	$i = $index + 1;
	$numSoftwareIDs ++;
}

if ($id == 'New')
{
	$imageNumber = 1;
	
	if ($imageEmpty)
		$imageNumber = -1;
	
	$query = "INSERT INTO projects (name, year_from, year_to, category_id, employer_id, description, link_name, link_url, image_number) VALUES ($name, $year1, $year2, $categoryID, $employerID, $description, $linkName, $linkURL, $imageNumber)";
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if (mysql_affected_rows() === 0)
		throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	$id = mysql_insert_id();
	
	if ($numLanguageIDs > 0)
	{
		$query = 'INSERT INTO project_languages (project_id, language_id) VALUES ';
		for ($i = 0; $i < $numLanguageIDs; $i++)
		{
			if ($i > 0)
				$query .= ', ';
			
			$query .= '(' . $id . ', ' . $languageIDs[$i] . ')';
		}
		
		$result = mysql_query($query);
			if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	}
	
	if ($numFrameworkIDs > 0)
	{
		$query = 'INSERT INTO project_frameworks (project_id, framework_id) VALUES ';
		for ($i = 0; $i < $numFrameworkIDs; $i++)
		{
			if ($i > 0)
				$query .= ', ';
			
			$query .= '(' . $id . ', ' . $frameworkIDs[$i] . ')';
		}
		
		$result = mysql_query($query);
			if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	}
	
	if ($numSoftwareIDs > 0)
	{
		$query = 'INSERT INTO project_software (project_id, software_id) VALUES ';
		for ($i = 0; $i < $numSoftwareIDs; $i++)
		{
			if ($i > 0)
				$query .= ', ';
			
			$query .= '(' . $id . ', ' . $softwareIDs[$i] . ')';
		}
		
		$result = mysql_query($query);
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	}
	
	if (!$imageEmpty)
	{
		if (file_exists('../images/projects/_temp' . IMAGE_EXT))
		{
			$filebasename = '../images/projects/' . $id . '_1';
			rename('../images/projects/_temp' . IMAGE_EXT, $filebasename . IMAGE_EXT);
			
			$sourceImage = imagecreatefrompng($filebasename . IMAGE_EXT);
			$width = imagesx($sourceImage);
			$height = imagesy($sourceImage);
			
			$nWidth = 0;
			$nHeight = 0;
			if ($width > 400 || $height > 200)
			{
				if (400 / $width < 200 / $height)
				{
					$nWidth = 400;
					$nHeight = $height * (400 / $width);
				}
				else
				{
					$nHeight = 200;
					$nWidth = $width * (200 / $height);
				}
			}
			
			$nImage = imagecreatetruecolor($nWidth, $nHeight);
			imagecopyresized($nImage, $sourceImage, 0, 0, 0, 0, $nWidth, $nHeight, $width, $height);
			
			imagepng($nImage, $filebasename . '_t' . IMAGE_EXT, 4);
		}
	}
	
	die('1');
}
else
{
	$origID = $id;
	$id = '\'' . mysql_real_escape_string($id) . '\'';
	
	$query = 'SELECT image_number FROM projects WHERE id = ' . $id;
	$result = mysql_query($query);
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	$imageNumber = (int)mysql_result($result, 0);
	
	if ($imageChanged)
	{
		if ($imageEmpty)
			$imageNumber = -abs($imageNumber);
		else
		{
			$imageNumber = abs($imageNumber) + 1;
			
			if ($imageNumber > 127)
				$imageNumber = 1;
			
			$filename1 = '../images/projects/_temp' . IMAGE_EXT;
			$filebasename2 = '../images/projects/' . $origID . '_' . $imageNumber;
			
			if (file_exists($filename1))
			{
				if (file_exists($filebasename2 . IMAGE_EXT))
					unlink($filebasename2 . IMAGE_EXT);
				
				if (file_exists($filebasename2 . '_t' . IMAGE_EXT))
					unlink($filebasename2 . '_t' . IMAGE_EXT);
				
				rename($filename1, $filebasename2 . IMAGE_EXT);
				
				$sourceImage = imagecreatefrompng($filebasename2 . IMAGE_EXT);
				$width = imagesx($sourceImage);
				$height = imagesy($sourceImage);
				
				$nWidth = 0;
				$nHeight = 0;
				if ($width > 400 || $height > 200)
				{
					if (400 / $width < 200 / $height)
					{
						$nWidth = 400;
						$nHeight = $height * (400 / $width);
					}
					else
					{
						$nHeight = 200;
						$nWidth = $width * (200 / $height);
					}
				}
				
				$nImage = imagecreatetruecolor($nWidth, $nHeight);
				imagecopyresized($nImage, $sourceImage, 0, 0, 0, 0, $nWidth, $nHeight, $width, $height);
				
				imagepng($nImage, $filebasename2 . '_t' . IMAGE_EXT, 4);
			}
		}
	}
	
	
	$query = "UPDATE projects SET name = $name, year_from = $year1, year_to = $year2, category_id = $categoryID, employer_id = $employerID, description = $description, link_name = $linkName, link_url = $linkURL, image_number = $imageNumber WHERE id = $id";
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	$query = 'DELETE FROM project_languages WHERE project_id = ' . $id;
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	$query = 'DELETE FROM project_frameworks WHERE project_id = ' . $id;
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	$query = 'DELETE FROM project_software WHERE project_id = ' . $id;
	$result = mysql_query($query);
	
	if ($result === false)
		throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	
	if ($numLanguageIDs > 0)
	{
		$query = 'INSERT INTO project_languages (project_id, language_id) VALUES ';
		for ($i = 0; $i < $numLanguageIDs; $i++)
		{
			if ($i > 0)
				$query .= ', ';
			
			$query .= '(' . $id . ', ' . $languageIDs[$i] . ')';
		}
		
		$result = mysql_query($query);
			if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	}
	
	if ($numFrameworkIDs > 0)
	{
		$query = 'INSERT INTO project_frameworks (project_id, framework_id) VALUES ';
		for ($i = 0; $i < $numFrameworkIDs; $i++)
		{
			if ($i > 0)
				$query .= ', ';
			
			$query .= '(' . $id . ', ' . $frameworkIDs[$i] . ')';
		}
		
		$result = mysql_query($query);
			if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	}
	
	if ($numSoftwareIDs > 0)
	{
		$query = 'INSERT INTO project_software (project_id, software_id) VALUES ';
		for ($i = 0; $i < $numSoftwareIDs; $i++)
		{
			if ($i > 0)
				$query .= ', ';
			
			$query .= '(' . $id . ', ' . $softwareIDs[$i] . ')';
		}
		
		$result = mysql_query($query);
			if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
	}
	
	die('1');
}
?>