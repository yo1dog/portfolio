<?php
ini_set('display_errors', '1');
ini_set('error_log', '../logs/errors.txt');

// ***********
// Disable Magic quotes
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}
// ***********

include('../includes/errors.php');
include('../includes/constants.php');
include('../includes/database.php');
include('../includes/FirePHPCore/fb.php');

$page = NULL;
$pageURL = NULL;
$title = NULL;
$exception = NULL;
$isScript = false;

if (isset($_GET['p']))
{
	$page = $_GET['p'];
	
	try
	{
		if ($page === 'adminMenu')
			$pageURL = 'pages/adminMenu.php';
		else if ($page === 'projects')
			$pageURL = 'pages/projects.php';
		else if ($page === 'manageComponents')
			$pageURL = 'pages/manageComponents.php';
		else if ($page === 'manageProjects')
			$pageURL = 'pages/manageProjects.php';
		else if ($page === 'manageComments')
			$pageURL = 'pages/manageComments.php';
		else
			throw new CustomException(PAGE_NOT_FOUND, __FILE__, __LINE__);
	}
	catch(Exception $e)
	{
		$exception = $e;
	}
}
else if (isset($_GET['s']))
{
	$isScript = true;
	$script = $_GET['s'];
	$headerFooter = false;
	
	try
	{
		if ($script === 'manageComponents')
			$pageURL = 'scripts/manageComponents.php';
		else if ($script === 'manageProjects')
			$pageURL = 'scripts/manageProjects.php';
		else if ($script === 'manageComments')
			$pageURL = 'scripts/manageComments.php';
		else if ($script === 'uploadImage')
			$pageURL = 'scripts/uploadImage.php';
		else if ($script === 'getProjects')
			$pageURL = 'scripts/getProjects.php';
		else
			throw new CustomException(PAGE_NOT_FOUND, __FILE__, __LINE__);
	}
	catch(Exception $e)
	{
		$exception = $e;
	}
}
else
	$pageURL = 'pages/adminMenu.php';

if ($exception !== NULL)
{
	if (get_class($exception) == 'CustomException')
	{
		if ($isScript)
			echo($exception);
		else
			$exception->GenerateMessage();
	}
	else
		echo($exception);
	
	error_log($exception);
}

if (!$isScript)
{
	?>

<html>
<head>
<title>Mike Moore - ADMIN</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<meta NAME="ROBOTS" CONTENT="All">
<link REL="styleSheet" HREF="style.css" TYPE="text/css">
<link REL="styleSheet" HREF="../styleShared.css" TYPE="text/css">
</head>

<body>

	<?php
}

try
{
	if ($pageURL !== NULL)
		include($pageURL);
}
catch(Exception $e)
{
	if (get_class($e) == 'CustomException')
	{
		if (!$isScript)
			$e->GenerateMessage();
		else
			$e->GenerateSimpleMessage();
	}
	else
		echo($e);
	
	error_log($e);
}
if (!$isScript)
{
	?>

</body>
</html>

	<?php
}
?>