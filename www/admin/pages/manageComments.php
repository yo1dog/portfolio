<script type="text/javascript" language="javascript">
var image = new Image();
image.src = "../images/ajaxLoader.gif";
</script>

<a href="?p=adminMenu">Admin Menu</a><br />
<br />

<?php
$query = 'SELECT c.id, p.name AS project_name, c.ip, c.name, c.text, c.timestamp, c.approved FROM project_comments AS c LEFT JOIN projects AS p ON (c.project_id = p.id) ORDER BY c.timestamp DESC';
$result = mysql_query($query);

if ($result === false)
	throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);

$pending = array();
$approved = array();
$denied = array();

while ($row = mysql_fetch_assoc($result))
{
	if ($row['approved'] === NULL)
		array_push($pending, $row);
	else if ($row['approved'] === '1')
		array_push($approved, $row);
	else
		array_push($denied, $row);
}

$numPending = count($pending);
$numApproved = count($approved);
$numDenied = count($denied);

echo('<h2>Pending</h2>');

if ($numPending === 0)
	echo('<span style="color:#F00">None</span>');
else
{
	echo('<table class="list" cellpadding="0" cellspacing="0"><tr><th>ID</th><th>Project</th><th>IP</th><th>Name</th><th>Timestamp</th></tr>');
	
	$alternate = false;
	for ($i = 0; $i < $numPending; $i++)
	{
		$rowClass = '';
		if ($alternate)
		{
			$alternate = false;
			$rowClass = ' class="alternate"';
		}
		else
			$alternate = true;
		
		$id = $pending[$i]['id'];
		echo('<tr' . $rowClass . '><td><a href="javascript:edit(' . $id . ')" >' .  $id . '</a></td><td><span style="white-space:nowrap">' . htmlspecialchars($pending[$i]['project_name']) . '</span></td><td>' . $pending[$i]['ip'] . '</td><td><span style="white-space:nowrap">' . htmlspecialchars($pending[$i]['name']) . '</span></td><td><span style="white-space:nowrap">' . $pending[$i]['timestamp'] . '</span></td></tr>');
	}
	
	echo('</table>');
}

echo('<br /><h2>Approved</h2>');

if ($numApproved === 0)
	echo('<span style="color:#F00">None</span>');
else
{
	echo('<table class="list" cellpadding="0" cellspacing="0"><tr><th>ID</th><th>Project</th><th>IP</th><th>Name</th><th>Timestamp</th></tr>');
	
	$alternate = false;
	for ($i = 0; $i < $numApproved; $i++)
	{
		$rowClass = '';
		if ($alternate)
		{
			$alternate = false;
			$rowClass = ' class="alternate"';
		}
		else
			$alternate = true;
		
		$id = $approved[$i]['id'];
		echo('<tr' . $rowClass . '><td><a href="javascript:edit(' . $id . ')" >' .  $id . '</a></td><td><span style="white-space:nowrap">' . htmlspecialchars($approved[$i]['project_name']) . '</span></td><td>' . $approved[$i]['ip'] . '</td><td><span style="white-space:nowrap">' . htmlspecialchars($approved[$i]['name']) . '</span></td><td><span style="white-space:nowrap">' . $approved[$i]['timestamp'] . '</span></td></tr>');
	}
	
	echo('</table>');
}

echo('<br /><h2>Denied</h2>');

if ($numDenied === 0)
	echo('<span style="color:#F00">None</span>');
else
{
	echo('<table class="list" cellpadding="0" cellspacing="0"><tr><th>ID</th><th>Project</th><th>IP</th><th>Name</th><th>Timestamp</th></tr>');
	
	$alternate = false;
	for ($i = 0; $i < $numDenied; $i++)
	{
		$rowClass = '';
		if ($alternate)
		{
			$alternate = false;
			$rowClass = ' class="alternate"';
		}
		else
			$alternate = true;
		
		$id = $denied[$i]['id'];
		echo('<tr' . $rowClass . '><td><a href="javascript:edit(' . $id . ')" >' .  $id . '</a></td><td><span style="white-space:nowrap">' . htmlspecialchars($denied[$i]['project_name']) . '</span></td><td>' . $denied[$i]['ip'] . '</td><td><span style="white-space:nowrap">' . htmlspecialchars($denied[$i]['name']) . '</span></td><td><span style="white-space:nowrap">' . $denied[$i]['timestamp'] . '</span></td></tr>');
	}
	
	echo('</table>');
}

?>

<br />
<br />
<div id="formWraper" style="display:none">
    <table class="fakeForm">
        <tr>
            <th scope="row">ID</th>
            <td><span id="id" style="font-style:italic;"></span></td>
        </tr>
        <tr>
            <th scope="row">Project</th>
            <td><span id="project" style="font-style:italic;"></span></td>
        </tr>
        <tr>
            <th scope="row">IP</th>
            <td><span id="ip" style="font-style:italic;"></span></td>
        </tr>
        <tr>
            <th scope="row">Timestamp</th>
            <td><span id="timestamp" style="font-style:italic;"></span></td>
        </tr>
        <tr>
            <th scope="row">Name</th>
            <td><input type="text" id="name" maxlength="<?php echo(NAME_CHARS); ?>" /></td>
        </tr>
        <tr>
            <th scope="row">Text</th>
            <td><textarea id="text" style="width:300px; height:150px;"></textarea></td>
        </tr>
        <tr>
            <th scope="row">Approved</th>
            <td><input type="radio" name="status" id="approved" /><label for="approved">Approved</label> <input type="radio" name="status" id="denied" /><label for="denied">Denied</label> <input type="radio" name="status" id="pending" /><label for="pending">Pending</label></td>
        </tr>
    </table>
    <input type="button" id="submitButton" onclick="formSubmit(0)" value="Update" /><input type="button" id="submitButtonDelete" value="Delete" onclick="deleteRow()" /><input type="button" id="submitButtonBlock" value="Block IP" onclick="blockIP()" /> <img id="submitLoading" src="../images/ajaxLoader.gif" style="display:none" /><span id="submitAlertText" class="alertText"></span>
</div>

<script type="text/javascript" language="javascript">

<?php
include('../includes/AJAX.js');

echo('var comments = [');

$first = true;
for ($i = 0; $i < $numPending; $i++)
{
	if ($first)
		$first = false;
	else
		echo(', ');
	
	echo('[' .  $pending[$i]['id'] . ', "' .  addslashes(htmlspecialchars($pending[$i]['project_name'])) . '", "' .  $pending[$i]['ip'] . '", "' .  addslashes($pending[$i]['name']) . '", "' . str_replace("\n", "\\n", str_replace("\r", "\\r", addslashes($pending[$i]['text']))) . '", "' .  $pending[$i]['timestamp'] . '", ' .  ($pending[$i]['approved'] === NULL? 'null' : $pending[$i]['approved']) . ']');
}

for ($i = 0; $i < $numApproved; $i++)
{
	if ($first)
		$first = false;
	else
		echo(', ');
	
	echo('[' .  $approved[$i]['id'] . ', "' .  addslashes(htmlspecialchars($approved[$i]['project_name'])) . '", "' .  $approved[$i]['ip'] . '", "' .  addslashes($approved[$i]['name']) . '", "' . str_replace("\n", "\\n", str_replace("\r", "\\r", addslashes($approved[$i]['text']))) . '", "' .  $approved[$i]['timestamp'] . '", ' .  ($approved[$i]['approved'] === NULL? 'null' : $approved[$i]['approved']) . ']');
}

for ($i = 0; $i < $numDenied; $i++)
{
	if ($first)
		$first = false;
	else
		echo(', ');
	
	echo('[' .  $denied[$i]['id'] . ', "' .  addslashes(htmlspecialchars($denied[$i]['project_name'])) . '", "' .  $denied[$i]['ip'] . '", "' .  addslashes($denied[$i]['name']) . '", "' . str_replace("\n", "\\n", str_replace("\r", "\\r", addslashes($denied[$i]['text']))) . '", "' .  $denied[$i]['timestamp'] . '", ' .  ($denied[$i]['approved'] === NULL? 'null' : $denied[$i]['approved']) . ']');
}

echo('];');
?>

function edit(id)
{
	var rowIndex;
	for (var i = 0; i < comments.length; i++)
	{
		if (comments[i][0] == id)
		{
			rowIndex = i;
			break;
		}
	}
	
	document.getElementById("formWraper").style.display = "block";
	document.getElementById("id").innerHTML = comments[rowIndex][0];
	document.getElementById("project").innerHTML = comments[rowIndex][1];
	document.getElementById("ip").innerHTML = comments[rowIndex][2];
	document.getElementById("timestamp").innerHTML = comments[rowIndex][5];
	document.getElementById("name").value = comments[rowIndex][3];
	document.getElementById("text").value = comments[rowIndex][4];
	document.getElementById("approved").checked = (comments[rowIndex][6] == 1? "checked" : "");
	document.getElementById("denied").checked = (comments[rowIndex][6] == 0? "checked" : "");
	document.getElementById("pending").checked = (comments[rowIndex][6] == null? "checked" : "");
}

function deleteRow()
{
	if (confirm("Are you sure you want to delete?"))
		formSubmit(1);
}

function blockIP()
{
	if (confirm("Are you sure you want to block this person's ip address?"))
		formSubmit(2);
}

var submitDisabled = false;
function formSubmit(action)
{
	if (submitDisabled)
		return false;
	
	var params = "id=" + document.getElementById("id").innerHTML;
	
	if (action == 1)
		params += "&delete=1";
	else if (action == 2)
		params += "&block=1&ip=" + document.getElementById("ip").innerHTML;
	else
	{
		params +=
			"&name=" + encodeURIComponent(document.getElementById("name").value.replace(/\+/g, "%2B")) +
			"&text=" + encodeURIComponent(document.getElementById("text").value.replace(/\+/g, "%2B")) +
			"&approved=";
		
		if (document.getElementById("approved").checked)
			params += "1";
		else if (document.getElementById("denied").checked)
			params += "0";
		else
			params += "null";
	}
	
	document.getElementById("submitAlertText").style.display = "none";
	document.getElementById("submitLoading").style.display = "inline";
	document.getElementById("submitButton").disabled = true;
	document.getElementById("submitButtonDelete").disabled = true;
	document.getElementById("submitButtonBlock").disabled = true;
	submitDisabled = true;
	
	AJAXHttpRequest(true, "?s=manageComments", params, formValidate);
	
	return false;
}

function formValidate(result)
{
	var submitAlertText = document.getElementById("submitAlertText");
	var submitLoading = document.getElementById("submitLoading");
	var submitButton = document.getElementById("submitButton");
	var submitButtonDelete = document.getElementById("submitButtonDelete");
	var submitButtonBlock = document.getElementById("submitButtonBlock");
	submitLoading.style.display = "none";
	
	if (result === 404)
	{
		submitAlertText.style.display = "inline";
		submitAlertText.innerHTML = "Error 404";
		
		submitButton.disabled = false;
		submitButtonDelete.disabled = false;
		submitButtonBlock.disabled = false;
		submitDisabled = false;
	}
	else if (result.charAt(0) === '1')
	{
		submitAlertText.style.display = "inline";
		submitAlertText.style.color = "009900";
		submitAlertText.innerHTML = "Success!";
		
		window.location = "?p=manageComments";
	}
	else
	{
		submitAlertText.style.display = "inline";
		submitAlertText.innerHTML = "Error";
		valid = false;
		
		submitButton.disabled = false;
		submitButtonDelete.disabled = false;
		submitButtonBlock.disabled = false;
		submitDisabled = false;
	}
}
</script>