<script type="text/javascript" language="javascript">
var image = new Image();
image.src = "../images/ajaxLoader.gif";
</script>

<a href="?p=adminMenu">Admin Menu</a><br />
<br />

<?php
$projects = Database::GetProjects(false);
$numProjects = count($projects);

echo('<table class="list" cellpadding="0" cellspacing="0"><tr><th>ID</th><th>Name</th><th>Year1</th><th>Year2</th><th>Category</th><th>Employer</th></tr>');

if ($numProjects == 0)
	echo('</table><span style="color:#F00">No Rows</span>');
else
{
	$alternate = false;
	for ($i = 0; $i < $numProjects; $i++)
	{
		$rowClass = '';
		if ($alternate)
		{
			$alternate = false;
			$rowClass = ' class="alternate"';
		}
		else
			$alternate = true;
		
		$year2 = $projects[$i]['year_to'];
		if ($year2 === NULL)
			$year2 = 'Present';
		else
			$year2 = htmlspecialchars($year2);
		
		echo('<tr' . $rowClass . '><td>' . $projects[$i]['id'] . '</td><td><span style="white-space:nowrap"><a href="javascript:edit(' . $i . ')">' . htmlspecialchars($projects[$i]['name']) . '</a></span></td><td><span style="white-space:nowrap">' . htmlspecialchars($projects[$i]['year_from']) . '</span></td><td><span style="white-space:nowrap">' . $year2 . '</span></td><td><span style="white-space:nowrap">' . htmlspecialchars($projects[$i]['category_name']) . '</span></td><td><span style="white-space:nowrap">' . htmlspecialchars($projects[$i]['employer_name']) . '</td></tr>');
	}
	
	echo('</table>');
}

$employers = Database::GetEmployersList();
$numEmployers = count($employers);
$languages = Database::GetLanguagesList();
$numLanguages = count($languages);
$frameworks = Database::GetFrameworksList();
$numFrameworks = count($frameworks);
$software = Database::GetSoftwareList();
$numSoftware = count($software);
$categories = Database::GetCategoriesList();
$numCategories = count($categories);
?>

<br />
<br />
<table class="fakeForm">
    <tr>
        <th scope="row">ID</th>
        <td><span id="id" style="font-style:italic;">New</span></td>
    </tr>
    <tr>
        <th scope="row">Name</th>
        <td><input type="text" id="name" maxlength="<?php echo(NAME_CHARS); ?>" /></td>
    </tr>
    <tr>
        <th scope="row">Year1</th>
        <td><input type="text" id="year1" maxlength="4" /></td>
    </tr>
    <tr>
        <th scope="row">Year2</th>
        <td><input type="text" id="year2" maxlength="4" /></td>
    </tr>
    <tr>
        <th scope="row">Category</th>
        <td>
            <select id="categoryID">
            
            <?php
            $categories = Database::GetCategories();
            $l = count($categories);
            
            for ($i = 0; $i < $l; $i++)
                echo('<option value="' . $categories[$i][0] . '">' . $categories[$i][1] . '</option>');
            ?>
            
            </select>
        </td>
    </tr>
    <tr>
        <th scope="row">Employer</th>
        <td>
            <select id="employerID">
            
            <?php
            $l = count($employers);
            
            for ($i = 0; $i < $l; $i++)
                echo('<option value="' . $employers[$i][0] . '">' . $employers[$i][1] . '</option>');
            ?>
            
            </select>
        </td>
    </tr>
    <tr>
    	<td colspan="2"><hr /></td>
    </tr>
    <tr>
        <th scope="row">Description</th>
        <td>
            <span style="white-space:nowrap; height:100%;">
                <textarea id="description" style="width:300px; height:150px;"></textarea><select multiple="multiple" style="width:100px; height:100%;">
                
                <?php
                for ($i = 0; $i < $numLanguages; $i++)
                    echo('<option ondblclick="insertItem(' . TABLE_LANGUAGES . ', ' . $languages[$i][0] . ', \'' . addslashes(htmlspecialchars($languages[$i][1])) . '\')">' . htmlspecialchars($languages[$i][1]) . '</option>');
                ?>
                
                </select><select multiple="multiple" style="width:100px; height:100%;">
                
                <?php
                for ($i = 0; $i < $numFrameworks; $i++)
                    echo('<option ondblclick="insertItem(' . TABLE_FRAMEWORKS . ', ' . $frameworks[$i][0] . ', \'' . addslashes(htmlspecialchars($frameworks[$i][1])) . '\')">' . htmlspecialchars($frameworks[$i][1]) . '</option>');
                ?>
                
                </select><select multiple="multiple" style="width:100px; height:100%;">
                
                <?php
                for ($i = 0; $i < $numSoftware; $i++)
                    echo('<option ondblclick="insertItem(' . TABLE_SOFTWARE . ', ' . $software[$i][0] . ', \'' . addslashes(htmlspecialchars($software[$i][1])) . '\')">' . htmlspecialchars($software[$i][1]) . '</option>');
                ?>
                
                </select><select multiple="multiple" style="width:100px; height:100%;">
                
                <?php
                for ($i = 0; $i < $numEmployers; $i++)
                    echo('<option ondblclick="insertItem(' . TABLE_EMPLOYERS . ', ' . $employers[$i][0] . ', \'' . addslashes(htmlspecialchars($employers[$i][1])) . '\')">' . htmlspecialchars($employers[$i][1]) . '</option>');
                ?>
                
                </select><select multiple="multiple" style="width:100px; height:100%;">
                
                <?php
                for ($i = 0; $i < $numCategories; $i++)
                    echo('<option ondblclick="insertItem(' . TABLE_CATEGORIES . ', ' . $categories[$i][0] . ', \'' . addslashes(htmlspecialchars($categories[$i][1])) . '\')">' . htmlspecialchars($categories[$i][1]) . '</option>');
                ?>
                
                </select><select multiple="multiple" style="width:100px; height:100%;">
                
                <?php
                for ($i = 0; $i < $numProjects; $i++)
                    echo('<option ondblclick="insertItem(' . TABLE_PROJECTS . ', ' . $projects[$i]['id'] . ', \'' . addslashes(htmlspecialchars($projects[$i]['name'])) . '\')">' . htmlspecialchars($projects[$i]['name']) . '</option>');
                ?>
                
                </select>
            </span>
        </td>
    </tr>
    <tr>
    	<td colspan="2"><hr /></td>
    </tr>
    <tr>
        <th scope="row">Link</th>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>Name</td>
                    <td style="padding-left:5px;">URL</td>
                </tr>
                <tr>
                    <td><input type="text" id="linkName" maxlength="<?php echo(NAME_CHARS); ?>" /></td>
                    <td style="padding-left:5px;"><input type="text" id="linkURL" maxlength="<?php echo(URL_CHARS); ?>" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2"><hr /></td>
    </tr>
    <tr>
        <th scope="row">Components</th>
        <td>
            <select id="languageIDs" multiple="multiple" style="height:200px;">
            
                <?php
                for ($i = 0; $i < $numLanguages; $i++)
                    echo('<option id="l' . $languages[$i][0] . '" value="' . $languages[$i][0] . '">' . htmlspecialchars($languages[$i][1]) . '</option>');
                ?>
            
            </select><select id="frameworkIDs" multiple="multiple" style="height:200px;">
            
                <?php
                for ($i = 0; $i < $numFrameworks; $i++)
                    echo('<option id="f' . $frameworks[$i][0] . '" value="' . $frameworks[$i][0] . '">' . htmlspecialchars($frameworks[$i][1]) . '</option>');
                ?>
            
            </select><select id="softwareIDs" multiple="multiple" style="height:200px;">
            
                <?php
                for ($i = 0; $i < $numSoftware; $i++)
                    echo('<option id="s' . $software[$i][0] . '" value="' . $software[$i][0] . '">' . htmlspecialchars($software[$i][1]) . '</option>');
                ?>
            
            </select>
        </td>
    </tr>
    <tr>
    	<td colspan="2"><hr /></td>
    </tr>
    <tr>
        <th scope="row">Image</th>
        <td>
            <table cellpadding="0" cellspacing="0" style="width:102px; height:102px;">
            	<tr>
                	<td style="text-align:center; vertical-align:middle;">
            			<a id="imageA" target="_blank"><img id="image" style="max-width:100px; max-height:100px; border: 1px solid #CCC;" src="../images/noImg.png" /></a>
            		</td>
                </tr>
            </table>
            <form id="uploadForm" action="?s=uploadImage" method="post" enctype="multipart/form-data" target="uploadTarget">
                <span style="white-space:nowrap"><input type="file" name="imageFile" />
                <input id="uploadSubmitButton" type="button" onclick="submitUploadForm()" value="Upload" /><input id="clearImageButton" type="button" onclick="clearImage()" value="Clear" /><input id="resetImageButton" type="button" onclick="resetImage()" value="Reset" style="display:none" />
                <img id="uploadSubmitLoading" src="../images/ajaxLoader.gif" style="display:none" /><span id="uploadSubmitAlertText" class="alertText">Upload Failed</span></span>
            </form>
            <iframe id="uploadTarget" name="uploadTarget"></iframe>
        </td>
    </tr>
</table>
<div class="bottomBar">
	<input type="button" id="submitButton" onclick="formSubmit(false)" value="Create" /><input type="button" id="submitButtonDelete" value="Delete" onclick="deleteRow()" style="display:none" /><input type="button" id="buttonNew" value="New" onclick="createNew()" style="display:none" />
    <img id="submitLoading" src="../images/ajaxLoader.gif" style="display:none" /><span id="submitAlertText" class="alertText"></span>
</div>

<script type="text/javascript" language="javascript">
<?php include('../includes/AJAX.js'); ?>
	
function insertItem(type, id, name)
{
	var description = document.getElementById("description");
	description.focus();
	
	var url = null;
	if (type === <?php echo(TABLE_PROJECTS); ?>)
		url = "javascript:setProject(" + id + ")";
	else if (type === <?php echo(TABLE_EMPLOYERS); ?>)
		url = "?p=employers#e" + id;
	else if (type === <?php echo(TABLE_LANGUAGES); ?>)
		url = "?p=projects&l=" + id;
	else if (type === <?php echo(TABLE_FRAMEWORKS); ?>)
		url = "?p=projects&f=" + id;
	else if (type === <?php echo(TABLE_SOFTWARE); ?>)
		url = "?p=projects&t=" + id;
	else if (type === <?php echo(TABLE_CATEGORIES); ?>)
		url = "?p=projects&c=" + id;
	
	if (description.selectionStart == description.selectionEnd)
		description.value = description.value.substr(0, description.selectionStart) + "<a href=\"" + url + "\">" + name + "</a>" + description.value.substr(description.selectionEnd);
	else
	{
		var strBegin = description.value.substr(0, description.selectionStart);
		var strReplace = description.value.substr(description.selectionStart, description.selectionEnd - description.selectionStart);
		var strEnd = description.value.substr(description.selectionEnd);
		
		while (strReplace.charAt(strReplace.length - 1) == ' ')
		{
			strReplace = strReplace.substr(0, strReplace.length - 1);
			strEnd = ' ' + strEnd;
		}
		
		description.value = strBegin + "<a href=\"" + url + "\">" + strReplace + "</a>" + strEnd;
	}
}

var imageEmpty = true;
var imageChanged = false;
var numGetTempImage = 0;
function getImage(id, imageNumber)
{
	if (id === null)
	{
		document.getElementById("image").src = "../images/ajaxLoader.gif";
		setTimeout("document.getElementById(\"image\").src = \"../images/projects/_temp<?php echo(IMAGE_EXT); ?>?" + numGetTempImage + "\";", 1);
		
		document.getElementById("imageA").href = "../images/projects/_temp<?php echo(IMAGE_EXT); ?>";
		
		numGetTempImage ++;
		imageChanged = true;
	}
	else
	{
		document.getElementById("image").src = "../images/ajaxLoader.gif";
		setTimeout("document.getElementById(\"image\").src = \"../images/projects/" + id + "_" + imageNumber + "<?php echo(IMAGE_EXT); ?>\";", 1);
		
		document.getElementById("imageA").href = "../images/projects/" + id + "_" + imageNumber + "<?php echo(IMAGE_EXT); ?>";
		
		imageChanged = false;
	}
	
	imageEmpty = false;
}

function clearImage()
{
	if (!imageEmpty)
	{
		document.getElementById("image").src = "../images/noImg.png";
		document.getElementById("imageA").href = "";
		
		imageChanged = true;
		imageEmpty = true;
	}
}
function resetImage()
{
	getImage(document.getElementById("id").innerHTML);
}

var uploadSubmitDisabled = false;
function submitUploadForm()
{
	if (uploadSubmitDisabled)
		return;
	
	uploadSubmitDisabled = true;
	document.getElementById("uploadSubmitButton").disabled = true;
	document.getElementById("clearImageButton").disabled = true;
	document.getElementById("resetImageButton").disabled = true;
	document.getElementById("uploadSubmitLoading").style.display = "inline";
	document.getElementById("uploadSubmitAlertText").style.display = "none";
	
	document.getElementById("image").src = "../images/ajaxLoader.gif";
	document.getElementById("imageA").href = "";
	
	document.getElementById("uploadForm").submit();
}

function uploadComplete(success)
{
	uploadSubmitDisabled = false;
	document.getElementById("uploadSubmitButton").disabled = false;
	document.getElementById("clearImageButton").disabled = false;
	document.getElementById("resetImageButton").disabled = false;
	document.getElementById("uploadSubmitLoading").style.display = "none";
	
	if (success)
		getImage(null, null);
	else
	{
		document.getElementById("uploadSubmitAlertText").style.display = "inline";
		clearImage();
	}
}

<?php
echo('var projects = [');
for ($i = 0; $i < $numProjects; $i++)
{
	if ($i > 0)
		echo(', ');
	
	echo('[' .  $projects[$i]['id'] . ', "' .  addslashes($projects[$i]['name']) . '", ' .  $projects[$i]['year_from'] . ', "' .  $projects[$i]['year_to'] . '", "' .  $projects[$i]['category_id'] . '", "' .  $projects[$i]['employer_id'] . '", "' .  str_replace("\n", "\\n", str_replace("\r", "\\r", addslashes($projects[$i]['description']))) . '", "' .  addslashes($projects[$i]['link_name']) . '", "' . addslashes($projects[$i]['link_url']) . '", ' . $projects[$i]['image_number'] . ', [');
	
	$l = count($projects[$i]['languages']);
	for ($j = 0; $j < $l; $j++)
	{
		if ($j > 0)
			echo(', ');
		
		echo($projects[$i]['languages'][$j][0]);
	}
	echo('], [');
	
	$l = count($projects[$i]['frameworks']);
	for ($j = 0; $j < $l; $j++)
	{
		if ($j > 0)
			echo(', ');
		
		echo($projects[$i]['frameworks'][$j][0]);
	}
	echo('], [');
	
	$l = count($projects[$i]['software']);
	for ($j = 0; $j < $l; $j++)
	{
		if ($j > 0)
			echo(', ');
		
		echo($projects[$i]['software'][$j][0]);
	}
	echo(']]');
}

echo('];');
?>

function edit(rowIndex)
{
	document.getElementById("id").innerHTML = projects[rowIndex][0];
	document.getElementById("name").value = projects[rowIndex][1];
	document.getElementById("year1").value = projects[rowIndex][2];
	document.getElementById("year2").value = projects[rowIndex][3];
	document.getElementById("categoryID").value = projects[rowIndex][4];
	document.getElementById("employerID").value = projects[rowIndex][5];
	document.getElementById("description").innerHTML = projects[rowIndex][6];
	document.getElementById("linkName").value = projects[rowIndex][7];
	document.getElementById("linkURL").value = projects[rowIndex][8];
	
	if (projects[rowIndex][9] > 0)
	{
		getImage(projects[rowIndex][0], projects[rowIndex][9]);
		document.getElementById("resetImageButton").style.display = "inline";
	}
	else
	{
		clearImage();
		document.getElementById("resetImageButton").style.display = "none";
	}
	
	imageChanged = false;
	
	var languageIDs = document.getElementById("languageIDs");
	for (var i = 0; i < languageIDs.options.length; i++)
		languageIDs.options[i].selected = "";
	
	var frameworkIDs = document.getElementById("frameworkIDs");
	for (var i = 0; i < frameworkIDs.options.length; i++)
		frameworkIDs.options[i].selected = "";
	
	var softwareIDs = document.getElementById("softwareIDs");
	for (var i = 0; i < softwareIDs.options.length; i++)
		softwareIDs.options[i].selected = "";
	
	for (var i = 0; i < projects[rowIndex][10].length; i++)
		document.getElementById("l" + projects[rowIndex][10][i]).selected = "selected";
	
	for (var i = 0; i < projects[rowIndex][11].length; i++)
		document.getElementById("f" + projects[rowIndex][11][i]).selected = "selected";
	
	for (var i = 0; i < projects[rowIndex][12].length; i++)
		document.getElementById("s" + projects[rowIndex][12][i]).selected = "selected";
	
	document.getElementById("submitButton").value = "Update";
	document.getElementById("submitButtonDelete").style.display = "inline";
	document.getElementById("buttonNew").style.display = "inline";
}

function createNew()
{
	if (!confirm("Creating a new project will disregard all changes. Are you sure?"))
		return;
	
	document.getElementById("id").innerHTML = "New";
	document.getElementById("name").value = "";
	document.getElementById("year1").value = "";
	document.getElementById("year2").value = "";
	document.getElementById("categoryID").value = 0;
	document.getElementById("employerID").value = 0;
	document.getElementById("description").innerHTML = "";
	document.getElementById("linkName").value = "";
	document.getElementById("linkURL").value = "";
	
	document.getElementById("resetImageButton").style.display = "none";
	clearImage();
	imageChanged = false;
	
	var languageIDs = document.getElementById("languageIDs");
	for (var i = 0; i < languageIDs.options.length; i++)
		languageIDs.options[i].selected = "";
	
	var frameworkIDs = document.getElementById("frameworkIDs");
	for (var i = 0; i < frameworkIDs.options.length; i++)
		frameworkIDs.options[i].selected = "";
	
	var softwareIDs = document.getElementById("softwareIDs");
	for (var i = 0; i < softwareIDs.options.length; i++)
		softwareIDs.options[i].selected = "";
	
	document.getElementById("submitButton").value = "Create";
	document.getElementById("submitButtonDelete").style.display = "none";
	document.getElementById("buttonNew").style.display = "none";
}

function deleteRow()
{
	if (confirm("Are you sure you want to delete?"))
		formSubmit(true);
}

var submitDisabled = false;
function formSubmit(bDelete)
{
	if (submitDisabled)
		return false;
	
	document.getElementById("submitAlertText").style.display = "none";
	document.getElementById("submitLoading").style.display = "inline";
	document.getElementById("submitButton").disabled = true;
	document.getElementById("submitButtonDelete").disabled = true;
	document.getElementById("buttonNew").disabled = true;
	submitDisabled = true;
	
	var params = "id=" + document.getElementById("id").innerHTML;
	
	if (bDelete)
		params += "&delete=1";
	else
	{
		params +=
			"&name=" + encodeURIComponent(document.getElementById("name").value.replace(/\+/g, "%2B")) +
			"&year1=" + encodeURIComponent(document.getElementById("year1").value.replace(/\+/g, "%2B")) +
			"&year2=" + encodeURIComponent(document.getElementById("year2").value.replace(/\+/g, "%2B")) +
			"&categoryID=" + encodeURIComponent(document.getElementById("categoryID").value.replace(/\+/g, "%2B")) +
			"&employerID=" + encodeURIComponent(document.getElementById("employerID").value.replace(/\+/g, "%2B")) +
			"&description=" + encodeURIComponent(document.getElementById("description").value.replace(/\+/g, "%2B")) +
			"&linkName=" + encodeURIComponent(document.getElementById("linkName").value.replace(/\+/g, "%2B")) +
			"&linkURL=" + encodeURIComponent(document.getElementById("linkURL").value.replace(/\+/g, "%2B")) +
			"&imageChanged=" + (imageChanged? "1" : "0") +
			"&imageEmpty=" + (imageEmpty? "1" : "0") +
			"&languageIDs=";
		
		var languageIDs = document.getElementById("languageIDs");
		for (var i = 0; i < languageIDs.options.length; i++)
		{
			if (languageIDs.options[i].selected)
				params += languageIDs.options[i].value + ",";
		}
		
		params += "&frameworkIDs=";
		var frameworkIDs = document.getElementById("frameworkIDs");
		for (var i = 0; i < frameworkIDs.options.length; i++)
		{
			if (frameworkIDs.options[i].selected)
				params += frameworkIDs.options[i].value + ",";
		}
		
		params += "&softwareIDs=";
		var softwareIDs = document.getElementById("softwareIDs");
		for (var i = 0; i < softwareIDs.options.length; i++)
		{
			if (softwareIDs.options[i].selected)
				params += softwareIDs.options[i].value + ",";
		}
	}
	
	AJAXHttpRequest(true, "?s=manageProjects", params, formValidate);
	
	return false;
}

function formValidate(result)
{
	var submitButton = document.getElementById("submitButton");
	var submitButtonDelete = document.getElementById("submitButtonDelete");
	var buttonNew = document.getElementById("buttonNew");
	var submitAlertText = document.getElementById("submitAlertText");
	var submitLoading = document.getElementById("submitLoading");
	submitLoading.style.display = "none";
	
	if (result === 404)
	{
		submitAlertText.style.display = "inline";
		submitAlertText.innerHTML = "Error 404";
		
		submitButton.disabled = false;
		submitButtonDelete.disabled = false;
		buttonNew.disabled = false;
		submitDisabled = false;
	}
	else if (result.charAt(0) === '1')
	{
		submitAlertText.style.display = "inline";
		submitAlertText.style.color = "009900";
		submitAlertText.innerHTML = "Success!";
		
		window.location = "?p=manageProjects";
	}
	else
	{
		submitAlertText.style.display = "inline";
		submitAlertText.innerHTML = "Error";
		valid = false;
		
		submitButton.disabled = false;
		submitButtonDelete.disabled = false;
		buttonNew.disabled = false;
		submitDisabled = false;
	}
}
</script>