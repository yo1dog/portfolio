<a href="?p=adminMenu">Admin Menu</a><br />
<br />

<?php
if (!isset($_GET['t']))
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);

$table = $_GET['t'];
$tableName = GetTableName($table);
$tableCols = GetTableColumns($table);
$numTableCols = count($tableCols) + 1;

$query = 'SELECT * FROM ' . $tableName;
$result = mysql_query($query);

if ($result === false)
	throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);

$rows = array();

echo('<table class="list" cellpadding="0" cellspacing="0"><tr><th>ID</th><th>Name</th>');
if ($table == TABLE_FRAMEWORKS || $table == TABLE_SOFTWARE)
	echo('<th style="width:auto">URL</th>');
else if ($table == TABLE_EMPLOYERS)
	echo('<th>Year1</th><th>Year2</th><th>Position</th><th>URL</th>');
echo('</tr>');

if (mysql_num_rows($result) == 0)
	echo('</table><span style="color:#F00">No Rows</span>');
else
{
	$alternate = false;
	while ($row = mysql_fetch_row($result))
	{
		array_push($rows, $row);
		
		$rowClass = '';
		if ($alternate)
		{
			$alternate = false;
			$rowClass = ' class="alternate"';
		}
		else
			$alternate = true;
		
		echo('<tr' . $rowClass . '><td>' . $row[0] . '</td><td><span style="white-space:nowrap"><a href="javascript:edit(' . (count($rows) - 1) . ')">' . htmlspecialchars($row[1]) . '</a></span></td>');
		
		if ($table == TABLE_FRAMEWORKS || $table == TABLE_SOFTWARE)
			echo('<td><span style="white-space:nowrap">' . htmlspecialchars($row[2]) . '</span></td>');
		else if ($table == TABLE_EMPLOYERS)
			echo('<td><span style="white-space:nowrap">' . htmlspecialchars($row[2]) . '</span></td><td><span style="white-space:nowrap">' . ($row[3] === NULL? 'Present' : htmlspecialchars($row[3])) . '</span></td><td><span style="white-space:nowrap">' . htmlspecialchars($row[4]) . '</span></td><td><span style="white-space:nowrap">' . htmlspecialchars($row[5]) . '</span></td>');
		
		echo('</tr>');
	}
	
	echo('</table>');
}

echo('<br />
<br />
<form onsubmit="return formSubmit(false);" method="get" style="width:1px">
	<fieldset>
		<table>
			<tr>
				<th scope="row" style="width:auto">ID</th>
				<td style="width:5px"><span id="col0" style="font-style:italic;">New</span></td>
			</tr>
			<tr>
				<th scope="row">Name</th>
				<td><input type="text" id="col1" maxlength="' . NAME_CHARS . '" /></td>
			</tr>');

if ($table == TABLE_FRAMEWORKS || $table == TABLE_SOFTWARE)
	echo('<tr><th scope="row">URL</th><td><input type="text" id="col2" maxlength="' . URL_CHARS . '" /></td></tr>');
else if ($table == TABLE_EMPLOYERS)
{
	echo('<tr><th scope="row">Year1</th><td><input type="text" id="col2" maxlength="4" /></td></tr><tr><th scope="row">Year2</th><td><input type="text" id="col3" maxlength="4" /></td></tr><tr><th scope="row">Position</th><td><input type="text" id="col4" maxlength="' . EMPLOYER_POSITION_CHARS . '" /></td></tr><tr><th scope="row">URL</th><td><input type="text" id="col5" maxlength="' . URL_CHARS . '" /></td></tr>');
}

echo('<tr><th scope="row">Desc</th><td><span style="white-space:nowrap; height:100%;"><textarea id="col' . ($numTableCols - 1) . '" style="width:300px; height:150px;"></textarea><select multiple="multiple" style="width:100px; height:100%;">');

$languages = Database::GetLanguagesList();
$frameworks = Database::GetFrameworksList();
$software = Database::GetSoftwareList();
$employers = Database::GetEmployersList();
$categories = Database::GetCategoriesList();
$projects = Database::GetProjectsList();

$l = count($languages);
for ($i = 0; $i < $l; $i++)
	echo('<option ondblclick="insertItem(' . TABLE_LANGUAGES . ', ' . $languages[$i][0] . ', \'' . addslashes(htmlspecialchars($languages[$i][1])) . '\')">' . htmlspecialchars($languages[$i][1]) . '</option>');

echo('</select><select multiple="multiple" style="width:100px; height:100%;">');

$l = count($frameworks);
for ($i = 0; $i < $l; $i++)
	echo('<option ondblclick="insertItem(' . TABLE_FRAMEWORKS . ', ' . $frameworks[$i][0] . ', \'' . addslashes(htmlspecialchars($frameworks[$i][1])) . '\')">' . htmlspecialchars($frameworks[$i][1]) . '</option>');

echo('</select><select multiple="multiple" style="width:100px; height:100%;">');

$l = count($software);
for ($i = 0; $i < $l; $i++)
	echo('<option ondblclick="insertItem(' . TABLE_SOFTWARE . ', ' . $software[$i][0] . ', \'' . addslashes(htmlspecialchars($software[$i][1])) . '\')">' . htmlspecialchars($software[$i][1]) . '</option>');

echo('</select><select multiple="multiple" style="width:100px; height:100%;">');

$l = count($employers);
for ($i = 0; $i < $l; $i++)
	echo('<option ondblclick="insertItem(' . TABLE_EMPLOYERS . ', ' . $employers[$i][0] . ', \'' . addslashes(htmlspecialchars($employers[$i][1])) . '\')">' . htmlspecialchars($employers[$i][1]) . '</option>');

echo('</select><select multiple="multiple" style="width:100px; height:100%;">');

$l = count($categories);
for ($i = 0; $i < $l; $i++)
	echo('<option ondblclick="insertItem(' . TABLE_CATEGORIES . ', ' . $categories[$i][0] . ', \'' . addslashes(htmlspecialchars($categories[$i][1])) . '\')">' . htmlspecialchars($categories[$i][1]) . '</option>');

echo('</select><select multiple="multiple" style="width:100px; height:100%;">');

$l = count($projects);
for ($i = 0; $i < $l; $i++)
	echo('<option ondblclick="insertItem(' . TABLE_PROJECTS . ', ' . $projects[$i][0] . ', \'' . addslashes(htmlspecialchars($projects[$i][1])) . '\')">' . htmlspecialchars($projects[$i][1]) . '</option>');

echo('</select></span></td></tr></table>
</fieldset>
<span style="white-space:nowrap"><input type="submit" id="submitButton" value="Create" /><input type="button" id="submitButtonDelete" value="Delete" onclick="deleteRow()" style="display:none" /><input type="button" id="buttonNew" value="New" onclick="createNew()" style="display:none" /></span><br />
<img id="submitLoading" src="../images/ajaxLoader.gif" style="display:none" /><span id="submitAlertText" class="alertText"></span></form>');
?>
<script type="text/javascript" language="javascript">

<?php include('../includes/AJAX.js'); ?>

function insertItem(type, id, name)
{
	var description = document.getElementById("col<?php echo($numTableCols - 1); ?>");
	description.focus();
	
	var url = null;
	if (type === <?php echo(TABLE_PROJECTS); ?>)
		url = "?p=projects&r=" + id;
	else if (type === <?php echo(TABLE_EMPLOYERS); ?>)
		url = "?p=employers#e" + id;
	else if (type === <?php echo(TABLE_LANGUAGES); ?>)
		url = "?p=projects&l=" + id;
	else if (type === <?php echo(TABLE_SOFTWARE); ?>)
		url = "?p=projects&t=" + id;
	else if (type === <?php echo(TABLE_FRAMEWORKS); ?>)
		url = "?p=projects&f=" + id;
	else if ($table == <?php echo(TABLE_CATEGORIES); ?>)
		url = "?p=projects&c=" + id;
	
	if (description.selectionStart == description.selectionEnd)
		description.value = description.value.substr(0, description.selectionStart) + "<a href=\"" + url + "\">" + name + "</a>" + description.value.substr(description.selectionEnd);
	else
	{
		var strBegin = description.value.substr(0, description.selectionStart);
		var strReplace = description.value.substr(description.selectionStart, description.selectionEnd - description.selectionStart);
		var strEnd = description.value.substr(description.selectionEnd);
		
		while (strReplace.charAt(strReplace.length - 1) == ' ')
		{
			strReplace = strReplace.substr(0, strReplace.length - 1);
			strEnd = ' ' + strEnd;
		}
		
		description.value = strBegin + "<a href=\"" + url + "\">" + strReplace + "</a>" + strEnd;
	}
}

<?php
echo('var rows = [');
$l = count($rows);
for ($i = 0; $i < $l; $i++)
{
	if ($i > 0)
		echo(', ');
	
	echo('[' .  $rows[$i][0]);
	
	for ($j = 1; $j < $numTableCols; $j++)
		echo(', "' . addslashes($rows[$i][$j]) . '"');
	
	echo(']');
}

echo('];');
?>


function edit(rowIndex)
{
	document.getElementById("col0").innerHTML = rows[rowIndex][0];
	
	<?php
	for ($i = 1; $i < $numTableCols; $i++)
		echo('document.getElementById("col' . $i . '").value = rows[rowIndex][' . $i . '];');
	?>
	
	document.getElementById("submitButton").value = "Update";
	document.getElementById("submitButtonDelete").style.display = "inline";
	document.getElementById("buttonNew").style.display = "inline";
}

function createNew()
{
	document.getElementById("col0").innerHTML = "New";
	
	<?php
	for ($i = 1; $i < $numTableCols; $i++)
		echo('document.getElementById("col' . $i . '").value = "";');
	?>
	
	document.getElementById("submitButton").value = "Create";
	document.getElementById("submitButtonDelete").style.display = "none";
	document.getElementById("buttonNew").style.display = "none";
}

function deleteRow()
{
	if (confirm("Are you sure you want to delete?"))
		formSubmit(true);
}

var submitDisabled = false;
function formSubmit(bDelete)
{
	if (submitDisabled)
		return false;
	
	document.getElementById("submitAlertText").style.display = "none";
	document.getElementById("submitLoading").style.display = "inline";
	document.getElementById("submitButton").disabled = true;
	document.getElementById("submitButtonDelete").disabled = true;
	document.getElementById("buttonNew").disabled = true;
	submitDisabled = true;
	
	var params = "&t=<?php echo($table); ?>&col0=" + document.getElementById("col0").innerHTML;
	
	if (bDelete)
		params += "&delete=1";
	else
		params += <?php
	for ($i = 1; $i < $numTableCols; $i++)
	{
		if ($i > 1)
			echo(' + ');
		
		echo('"&col' . $i . '=" + encodeURIComponent(document.getElementById("col' . $i . '").value.replace(/\+/g, "%2B"))');
	}
	?>;
	
	AJAXHttpRequest(true, "?s=manageComponents", params, formValidate);
	
	return false;
}

function formValidate(result)
{
	var submitButton = document.getElementById("submitButton");
	var submitButtonDelete = document.getElementById("submitButtonDelete");
	var buttonNew = document.getElementById("buttonNew");
	var submitAlertText = document.getElementById("submitAlertText");
	var submitLoading = document.getElementById("submitLoading");
	submitLoading.style.display = "none";
	
	if (result === 404)
	{
		submitAlertText.style.display = "inline";
		submitAlertText.innerHTML = "Error 404";
		
		submitButton.disabled = false;
		submitButtonDelete.disabled = false;
		buttonNew.disabled = false;
		submitDisabled = false;
	}
	else if (result.charAt(0) === '1')
	{
		submitAlertText.style.display = "inline";
		submitAlertText.style.color = "009900";
		submitAlertText.innerHTML = "Success!";
		
		window.location = "?p=manageComponents&t=<?php echo($table); ?>";
	}
	else
	{
		submitAlertText.style.display = "inline";
		submitAlertText.innerHTML = "Error";
		valid = false;
		
		submitButton.disabled = false;
		submitButtonDelete.disabled = false;
		buttonNew.disabled = false;
		submitDisabled = false;
	}
}
</script>