<?php
if (!isset($_POST['r']) || !isset($_POST['n']) || !isset($_POST['t']))
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);

$projectID = $_POST['r'];
$name = $_POST['n'];
$text = $_POST['t'];

if (strlen($name) < 2 || strlen($text) < 10)
	die('0');

setcookie('COMMENT_NAME', $name, time() + 2592000);

$ip = $_SERVER['REMOTE_ADDR'];

$query = 'SELECT EXISTS(SELECT 1 FROM project_comments_blocked_ips WHERE ip = \'' . $ip . '\'), EXISTS(SELECT 1 FROM project_comments WHERE ip = \'' . $ip . '\' AND approved IS NULL)';
$result = mysql_query($query);

if ($result === false)
	throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);

if (mysql_result($result, 0, 0) === '1')
	die('b');
if (mysql_result($result, 0, 1) === '1')
	die('p');

$query = 'INSERT INTO project_comments (project_id, ip, name, text) VALUES (\'' . mysql_real_escape_string($projectID) . '\', \'' . $ip . '\', \'' . mysql_real_escape_string($name) . '\', \'' . mysql_real_escape_string($text) . '\')';
$result = mysql_query($query);

if ($result === false)
	throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);

if (mysql_affected_rows() === 0)
	throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);

echo('1');

mail('mike@mikeem.com', 'Comment Posted', '<html>
	<head>
  		<title>Comment Posted</title>
	</head>
	<body>
		A comment has been posted and is pending approval.<br />
		<br />
		<a href="http://admin.mikeem.com?p=manageComments">http://admin.mikeem.com?p=manageComments</a>
	</body>
</html>', 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=iso-8859-1' . "\r\n" . 'From: MikeEM.com <comments@mikeem.com>' . "\r\n");
?>