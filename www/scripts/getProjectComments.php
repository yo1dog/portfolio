<?php
if (!isset($_POST['r']))
	throw new CustomException(MISSING_URL_PARAMETER, __FILE__, __LINE__);

$projectID = $_POST['r'];

$comments = Database::GetProjectComments($projectID);
$numComments = count($comments);

$months = array(
	'01' => 'January',
	'02' => 'Febuary',
	'03' => 'March',
	'04' => 'April',
	'05' => 'May',
	'06' => 'June',
	'07' => 'July',
	'08' => 'August',
	'09' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December');

echo('<table cellpadding="0" cellspacing="0" class="commentWraper">');

for ($i = 0; $i < $numComments; $i++)
{
	$timestamp = $comments[$i]['timestamp'];
	
	$hour = (int)substr($timestamp, 11, 2);
	$ap = NULL;
	
	if ($hour < 12)
	{
		if ($hour === 0)
			$hour = 12;
		
		$ap = 'am';
	}
	else
	{
		if ($hour > 12)
			$hour -= 12;
		
		$ap = 'pm';
	}
	
	echo('<tr><td><img src="images/spacer.png" height="' . ($i > 0? '5' : '10') . '" /></td></tr><tr class="commentRow"><td><div class="name">' . htmlspecialchars($comments[$i]['name']) . '</div><div class="timestamp">' . $months[substr($timestamp, 5, 2)] . ' ' . ((int)substr($timestamp, 8, 2)) . ', ' . substr($timestamp, 0, 4) . ' at ' . $hour . ':' . substr($timestamp, 14, 2) . ' ' . $ap . '</div><br /><div class="text">' . htmlspecialchars($comments[$i]['text']) . '</div></td></tr>');
}

echo('</table>');