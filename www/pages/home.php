<center>
	<img src="images/banner.jpg" />
</center>
<br />

<h1>About Me</h1>
<p>I love programming. I spend most of my free time working on personal projects. When I'm not programming I'm
usually tinkering with hardware or building something in my shop. My interests include physics simulations,
game development, 3D modeling, computer hardware, boolean algebra, embeded systems, general EE, and carpentry.</p>
<br />

<h1>What Can I Do For You?</h1>
<p>Whatever you need me to do. I am fast yet meticulous and I hold all of my work to a very high standard.
I enjoy learning new languages and frameworks and can pick them up quickly. I have years of experience working
with companies and teams both large and small. I am a good communicator and have experience both dealing with
clients directly and as a team leader.</p>
<p>I have built and worked on many servers and architected everything from the database to the API. This experience allows me
to plan and build servers that are fast, reliable, maintainable, easily extendable, scaleable... just plain able.
Not exactly sure what you want? No problem. I have experience working at startups and groups with quickly changing products.
With a focus on the value of your data (and especially in the relationships between your data) we can build a server
that can change just as fast without compromising integrity.<p>
<p>Most of my experience consists of back-end work with Java/Tomcat/PostgreSQL, Node.js/Express/MongoDB, and PHP/Apache/MySQL.
I have done some client-side work, but that is not my forte. I try to keep up
with the latest HTML5 and CSS3 features and can throw some decent webpages together. I am very proficient in JavaScript
(vanilla, jQuery, AJAX) and can write web-based tools. I have done some development on iOS and Android apps.</p>