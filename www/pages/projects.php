<script type="text/javascript" language="javascript">
var image = new Image();
image.src = "../images/ajaxLoaderBig.gif";
</script>

<?php
$projects = Database::GetProjects(true);
$numProjects = count($projects);

// categories
$categories = Database::GetCategoriesList();
$numCategories = count($categories);

$filterCategories = array(0 => true);
for ($i = 0; $i < $numCategories; $i++)
	$filterCategories[$categories[$i][0]] = true;

if (isset($_GET['c']))
{
	$params = explode(',', $_GET['c']);
	
	$filterCategories[0] = false;
	foreach ($filterCategories as $id => $value)
		$filterCategories[$id] = false;
	
	$l = count($params);
	for ($i = 0; $i < $l; $i++)
		$filterCategories[$params[$i]] = true;
}

// employers
$employers = Database::GetEmployersList();
$numEmployers = count($employers);

$filterEmployers = array(0 => true);
for ($i = 0; $i < $numEmployers; $i++)
	$filterEmployers[$employers[$i][0]] = true;

if (isset($_GET['e']))
{
	$params = explode(',', $_GET['e']);
	
	$filterEmployers[0] = false;
	foreach ($filterEmployers as $id => $value)
		$filterEmployers[$id] = false;
	
	$l = count($params);
	for ($i = 0; $i < $l; $i++)
		$filterEmployers[$params[$i]] = true;
}

// languages
$languages = Database::GetLanguagesList();
$numLanguages = count($languages);

$filterLanguages = array(0 => true);
for ($i = 0; $i < $numLanguages; $i++)
	$filterLanguages[$languages[$i][0]] = true;

if (isset($_GET['l']))
{
	$params = explode(',', $_GET['l']);
	
	$filterLanguages[0] = false;
	foreach ($filterLanguages as $id => $value)
		$filterLanguages[$id] = false;
	
	$l = count($params);
	for ($i = 0; $i < $l; $i++)
		$filterLanguages[$params[$i]] = true;
}

// frameworks
$frameworks = Database::GetFrameworksList();
$numFrameworks = count($frameworks);

$filterFrameworks = array(0 => true);
for ($i = 0; $i < $numFrameworks; $i++)
	$filterFrameworks[$frameworks[$i][0]] = true;

if (isset($_GET['f']))
{
	$params = explode(',', $_GET['f']);
	
	$filterFrameworks[0] = false;
	foreach ($filterFrameworks as $id => $value)
		$filterFrameworks[$id] = false;
	
	$l = count($params);
	for ($i = 0; $i < $l; $i++)
		$filterFrameworks[$params[$i]] = true;
}

// software
$software = Database::GetSoftwareList();
$numSoftware = count($software);

$filterSoftware = array(0 => true);
for ($i = 0; $i < $numSoftware; $i++)
	$filterSoftware[$software[$i][0]] = true;

if (isset($_GET['t']))
{
	$params = explode(',', $_GET['t']);
	
	$filterSoftware[0] = false;
	foreach ($filterSoftware as $id => $value)
		$filterSoftware[$id] = false;
	
	$l = count($params);
	for ($i = 0; $i < $l; $i++)
		$filterSoftware[$params[$i]] = true;
}

// project
$prjectID = NULL;
if (isset($_GET['r']))
	$prjectID = $_GET['r'];

echo('
<table cellpadding="0" cellspacing="0" style="width:100%">
	<tr>
		<td colspan="2">
			<h1>Projects</h1>
		</td>
	</tr>
	<tr>
		<td class="filterOptions">
			<div style="height:1px"><img src="images/spacer.png" width="130" height="1"/></div>
			<table cellpadding="0" cellspacing="0" style="width:100%">
				<tr>
					<td colspan="2" class="title">Categories</td>
				</tr>');
			
for ($i = 0; $i < $numCategories; $i++)
{
	$id = $categories[$i][0];
	echo('<tr><td><input type="checkbox" name="cB" id="c' . $id . '" onclick="checkboxChanged()"');
	
	if ($filterCategories[$id])
		echo(' checked="checked"');
	
	echo(' /></td><td><label for="c' . $id . '">' . $categories[$i][1] . '</label></td></tr>');
}

echo('<tr><td colspan="2"><a href="javascript:checkAll(\'c\', true)"><img src="images/check.png" /></a> / <a href="javascript:checkAll(\'c\', false)"><img src="images/uncheck.png" /></a> All</td></tr><tr><td colspan="2" class="title"><br />Employers</td></tr>');

for ($i = 0; $i < $numEmployers; $i++)
{
	$id = $employers[$i][0];
	echo('<tr><td><input type="checkbox" name="eB" id="e' . $id . '" onclick="checkboxChanged()"');
	
	if ($filterEmployers[$id])
		echo(' checked="checked"');
	
	echo(' /></td><td><label for="e' . $id . '">' . $employers[$i][1] . '</label></td></tr>');
}

echo('<tr><td colspan="2"><a href="javascript:checkAll(\'e\', true)"><img src="images/check.png" /></a> / <a href="javascript:checkAll(\'e\', false)"><img src="images/uncheck.png" /></a> All</td></tr><tr><td colspan="2" class="title"><br />Languages</span></td></tr>');

for ($i = 0; $i < $numLanguages; $i++)
{
	$id = $languages[$i][0];
	echo('<tr><td><input type="checkbox" name="lB" id="l' . $id . '" onclick="checkboxChanged()"');
	
	if ($filterLanguages[$id])
		echo(' checked="checked"');
	
	echo(' /></td><td><label for="l' . $id . '">' . $languages[$i][1] . '</label></td></tr>');
}

echo('<tr><td><input type="checkbox" name="lB" id="l0" onclick="checkboxChanged()"');
if ($filterLanguages[0])
	echo(' checked="checked"');
echo(' /></td><td><label for="l0">None</label></td></tr><tr><td colspan="2"><a href="javascript:checkAll(\'l\', true)"><img src="images/check.png" /></a> / <a href="javascript:checkAll(\'l\', false)"><img src="images/uncheck.png" /></a> All</td></tr><tr><td colspan="2" class="title"><br />Frameworks</span></td></tr>');

for ($i = 0; $i < $numFrameworks; $i++)
{
	$id = $frameworks[$i][0];
	echo('<tr><td><input type="checkbox" name="fB" id="f' . $id . '" onclick="checkboxChanged()"');
	
	if ($filterFrameworks[$id])
		echo(' checked="checked"');
	
	echo(' /></td><td><label for="f' . $id . '">' . $frameworks[$i][1] . '</label></td></tr>');
}

echo('<tr><td><input type="checkbox" name="fB" id="f0" onclick="checkboxChanged()"');
if ($filterFrameworks[0])
	echo(' checked="checked"');
echo(' /></td><td><label for="f0">None</label></td></tr><tr><td colspan="2"><a href="javascript:checkAll(\'f\', true)"><img src="images/check.png" /></a> / <a href="javascript:checkAll(\'f\', false)"><img src="images/uncheck.png" /></a> All</td></tr><tr><td colspan="2" class="title"><br />Software</td></tr>');

for ($i = 0; $i < $numSoftware; $i++)
{
	$id = $software[$i][0];
	echo('<tr><td><input type="checkbox" name="sB" id="s' . $id . '" onclick="checkboxChanged()"');
	
	if ($filterSoftware[$id])
		echo(' checked="checked"');
	
	echo(' /></td><td><label for="s' . $id . '">' . $software[$i][1] . '</label></td></tr>');
}
	
echo('<tr><td><input type="checkbox" name="sB" id="s0" onclick="checkboxChanged()"');
if ($filterSoftware[0])
	echo(' checked="checked"');
echo(' /></td><td><label for="s0">None</label></td></tr><tr><td colspan="2"><a href="javascript:checkAll(\'s\', true)"><img src="images/check.png" /></a> / <a href="javascript:checkAll(\'s\', false)"><img src="images/uncheck.png" /></a> All</td></tr></table></td><td style="width:100%; vertical-align:top;"><div id="projectsContent"></div></td></tr></table>');
?>

<script type="text/javascript" language="javascript">

<?php
include('includes/AJAX.js');

echo('var projects = [');

for ($i = 0; $i < $numProjects; $i++)
{
	if ($i > 0)
		echo("\n" . ', ');
	
	$id = $projects[$i]['id'];
	$name = '"' . addslashes(htmlspecialchars($projects[$i]['name'])) . '"';
	$yearFrom = $projects[$i]['year_from'];
	$yearTo = $projects[$i]['year_to']; if ($yearTo === NULL) $yearTo = 'null';
	$categoryID = $projects[$i]['category_id']; if ($categoryID === NULL) $categoryID = 'null';
	$employerID = $projects[$i]['employer_id']; if ($employerID === NULL) $employerID = 'null';
	$employerName = $projects[$i]['employer_name']; if ($employerName === NULL) $employerName = 'null'; else $employerName = '"' . addslashes(htmlspecialchars($employerName)) . '"';
	$description = '"' . str_replace("\n", "", str_replace("\r", "", nl2br(addslashes($projects[$i]['description'])))) . '"';
	$linkName = $projects[$i]['link_name']; if ($linkName === NULL) $linkName = 'null'; else $linkName = '"' . addslashes(htmlspecialchars($linkName)) . '"';
	$linkURL = $projects[$i]['link_url']; if ($linkURL === NULL) $linkURL = 'null'; else $linkURL = '"' . addslashes($linkURL) . '"';
	$imageNumber = $projects[$i]['image_number'];
	
	echo('[' . $id . ', ' . $name . ', ' . $yearFrom . ', ' . $yearTo . ', ' . $categoryID . ', ' . $employerID . ', ' . $employerName . ', ' . $description . ', ' . $linkName . ', ' . $linkURL . ', ' . $imageNumber . ', [');
	
	$l = count($projects[$i]['languages']);
	for ($j = 0; $j < $l; $j++)
	{
		if ($j > 0)
			echo(', ');
		
		echo('[' . $projects[$i]['languages'][$j][0] . ', "' . addslashes(htmlspecialchars($projects[$i]['languages'][$j][1])) . '"]');
	}
	
	echo('], [');
	
	$l = count($projects[$i]['frameworks']);
	for ($j = 0; $j < $l; $j++)
	{
		if ($j > 0)
			echo(', ');
		
		echo('[' . $projects[$i]['frameworks'][$j][0] . ', "' . addslashes(htmlspecialchars($projects[$i]['frameworks'][$j][1])) . '"]');
	}
	
	echo('], [');
	
	$l = count($projects[$i]['software']);
	for ($j = 0; $j < $l; $j++)
	{
		if ($j > 0)
			echo(', ');
		
		echo('[' . $projects[$i]['software'][$j][0] . ', "' . addslashes(htmlspecialchars($projects[$i]['software'][$j][1])) . '"]');
	}
	
	echo(']]');
}

echo('];');
?>

function checkAll(component, checked)
{
	var checkboxes = document.getElementsByName(component + 'B');
	var checkedValue = "";
	
	if (checked)
		checkedValue = "checked";
	
	for (var i = 0; i < checkboxes.length; i++)
		checkboxes[i].checked = checkedValue;
	
	update(null);
}

var gProjectID = null;
function update(projectID)
{
	gProjectID = projectID;
	
	var html = "";
	
	if (projectID !== null)
		html += '<tr><td class="returnLink" style="padding-bottom:10px"><a href="javascript:showAll()">Return to Search</a></td></tr>';
	
	var first = true;
	
	for (var i = 0; i < projects.length; i++)
	{
		if (projectID === null)
		{
			if (!document.getElementById("c" + projects[i][4]).checked ||
				!document.getElementById("e" + projects[i][5]).checked)
				continue;
			
			var skip = true;
			
			if (projects[i][11].length == 0)
				skip = !document.getElementById("l0").checked;
			else
			{
				for (var j = 0; j < projects[i][11].length; j++)
				{
					if (document.getElementById("l" + projects[i][11][j][0]).checked)
					{
						skip = false;
						break;
					}
				}
			}
			
			if (skip)
				continue;
			
			skip = true;
			
			if (projects[i][12].length == 0)
				skip = !document.getElementById("f0").checked;
			else
			{
				for (var j = 0; j < projects[i][12].length; j++)
				{
					if (document.getElementById("f" + projects[i][12][j][0]).checked)
					{
						skip = false;
						break;
					}
				}
			}
			
			if (skip)
				continue;
			
			skip = true;
			
			if (projects[i][13].length == 0)
				skip = !document.getElementById("s0").checked;
			else
			{
				for (var j = 0; j < projects[i][13].length; j++)
				{
					if (document.getElementById("s" + projects[i][13][j][0]).checked)
					{
						skip = false;
						break;
					}
				}
			}
			
			if (skip)
				continue;
		}
		else
		{
			if (projects[i][0] != projectID)
				continue;
		}
		
		html += '<tr style="';
		
		if (first)
		{
			html += 'display:none';
			first = false;
		}
		else
			html += 'height:40px';
		
		html += '"><td><a name="' + projects[i][0] + '"></a>&nbsp;</td></tr><tr class="projectContentRow"><td class="projectContentCell"><h4>' + projects[i][1] + '</h4><h5>' + (projects[i][2] == projects[i][3]?  projects[i][2] : projects[i][2] + ' - ' + (projects[i][3] === null? 'Present' : projects[i][3])) + '<br />' + projects[i][6] + '</h5>';
		
		if (projects[i][10] > 0)
		{
			var urlbase = 'images/projects/' + projects[i][0] + '_' + projects[i][10];
			html += '<table cellpadding="0" cellspacing="0" style="width:auto; height:auto; border:3px solid #CCC; padding: 2px;"><tr><td><a href="' + urlbase + '<?php echo(IMAGE_EXT); ?>" target="_blank"><img src="' + urlbase + '_t<?php echo(IMAGE_EXT); ?>" /></a></td></tr></table><br /><br />';
		}
		
		html += projects[i][7];
		
		if (projects[i][9] !== null)
		{
			html += '<br /><br /><br /><a href="' + projects[i][9] + '" class="link" target="_blank">';
			
			if (projects[i][8] === null)
				html += 'Link';
			else
				html += projects[i][8];
			
			html += '</a><br />';
		}
		
		var componentsString = '';
	
		if (projects[i][11].length > 0)
		{
			componentsString += '<td style="width:auto">Languages<br /><ul>';
			
			for (var j = 0; j < projects[i][11].length; j++)
				componentsString += '<li><a href="?p=experience#l' + projects[i][11][j][0] + '">' + projects[i][11][j][1] + '</a></li>';
			
			componentsString += '</ul></td>';
		}
		
		if (projects[i][12].length > 0)
		{
			componentsString += '<td style="width:auto">Frameworks<br /><ul>';
			
			for (var j = 0; j < projects[i][12].length; j++)
				componentsString += '<li><a href="?p=experience#l' + projects[i][12][j][0] + '">' + projects[i][12][j][1] + '</a></li>';
			
			componentsString += '</ul></td>';
		}
		
		if (projects[i][13].length > 0)
		{
			componentsString += '<td style="width:auto">Software<br /><ul>';
			
			for (var j = 0; j < projects[i][13].length; j++)
				componentsString += '<li><a href="?p=experience#l' + projects[i][13][j][0] + '">' + projects[i][13][j][1] + '</a></li>';
			
			componentsString += '</ul></td>';
		}
		
		if (componentsString.length > 0)
			html += '<br /><br /><table class="componentsTable"><tr>' + componentsString + '<td style="width:100%"></td></tr></table>';
		
		if (projectID === null)
			html += '<br /><a href="javascript:setProject(' + projects[i][0] + ')">View Comments</a>';
		
		html += '</td></tr>';
	}
	
	if (first)
	{
		html = '<tr><td class="noRows">';
		
		if (projectID === null)
			html += 'There are no projects that fit that criteria!';
		else
			html += 'Project not found!';
		
		html += '</td></tr>';
	}
	
	if (projectID !== null)
		html += '<tr><td class="returnLink" style="padding-top:10px; padding-bottom:10px"><a href="javascript:showAll()">Return to Search</a></td></tr><tr class="postCommentRow"><td class="postCommentCell" id="postCommentCell"><div class="commentWraper"><span id="postCommentTitle" class="title">Post a Comment</span><table id="postCommentForm" cellpadding="0" cellspacing="0" style="width:100%;" class="postCommentForm"><tr><td colspan="2" id="postCommentText" class="text"><div style="height:100px"><textarea id="commentText"></textarea></div></td></tr><tr><td id="postCommentName" style="width:100%" class="name">Name: <input type="text" id="commentName" maxlength="<?php echo(NAME_CHARS); ?>" value="<?php if (isset($_COOKIE['COMMENT_NAME'])) echo($_COOKIE['COMMENT_NAME']); ?>" /> <span class="alertText" id="alertText"></span></td><td id="postCommentButton" style="width:auto" class="button"><input type="button" onclick="postComment()" value="Post" id="submitButton" /></td></tr></table></div></td></tr><tr><td id="commentsContent"><div style="text-align:center; padding-top: 10px;" class="commentWraper">Loading Comments...<br /><img src="images/ajaxLoaderBig.gif" /></div></td></tr><tr><td class="returnLink" style="padding-top:10px; padding-bottom:10px"><a href="javascript:showAll()">Return to Search</a></td></tr>';
	
	document.getElementById("projectsContent").innerHTML = '<table cellpadding="0" cellspacing="0" class="projects">' + html + '</table>';
	
	if (projectID !== null)
		getProjectComments();
}

function checkboxChanged()
{
	update(null);
}

function setProject(projectID)
{
	update(projectID);
}

function showAll()
{
	update(null);
}

function getProjectComments()
{
	AJAXHttpRequest(true, "?s=getProjectComments", "r=" + gProjectID, getProjectCommentsValidate, gProjectID, true);
}

function getProjectCommentsValidate(result, params, sentHttp)
{
	if (sentHttp == lastHttp && gProjectID == params)
		document.getElementById("commentsContent").innerHTML = '<table cellpadding="0" cellspacing="0" class="comments">' + result + '</table>';
}

var submitDisabled = false;
function postComment()
{
	if (submitDisabled)
		return;
	
	var commentName = document.getElementById("commentName");
	var commentText = document.getElementById("commentText");
	var alertText = document.getElementById("alertText");
	
	if (commentName.value.length == 0)
	{
		alertText.style.display = "inline";
		alertText.innerHTML = "Please enter your name";
		
		return;
	}
	
	if (commentName.value.length < 2)
	{
		alertText.style.display = "inline";
		alertText.innerHTML = "Your name must be at least 2 characters";
		
		return;
	}
	
	if (commentName.value.length > <?php echo(NAME_CHARS);?>)
	{
		alertText.style.display = "inline";
		alertText.innerHTML = "Your name must be less than <?php echo(NAME_CHARS);?> characters";
		
		return;
	}
	
	if (commentText.value.length < 10)
	{
		alertText.style.display = "inline";
		alertText.innerHTML = "Your comment must be at least 10 characters";
		
		return;
	}
	
	if (commentText.value.length > <?php echo(COMMENT_TEXT_CHARS);?>)
	{
		alertText.style.display = "inline";
		alertText.innerHTML = "Your comment must be less than <?php echo(COMMENT_TEXT_CHARS);?> characters";
		
		return;
	}
	
	
	submitDisabled = true;
	alertText.style.display = "none";
	
	var submitButton = document.getElementById("submitButton");
	submitButton.disabled = true;
	submitButton.value = "Posting...";
	
	AJAXHttpRequest(true, "?s=postComment", "r=" + gProjectID + "&n=" + encodeURIComponent(commentName.value) + "&t=" + encodeURIComponent(commentText.value), postCommentValidate);
}

function postCommentValidate(result)
{
	submitDisabled = false;
	
	if (result[0] === '1')
	{
		var postCommentTitle = document.getElementById("postCommentTitle");
		postCommentTitle.innerHTML = "Thanks for commenting!";
		postCommentTitle.style.textAlign = "center";
		postCommentTitle.style.fontWeight = "bold";
		postCommentTitle.style.color = "#060";
		
		var postCommentText = document.getElementById("postCommentText");
		postCommentText.innerHTML = '<div style="height:100px"><br />Your comment has been submited and is pending approval.<br />It will be displayed after it has been approved.</div>';
		postCommentText.style.borderColor = "transparent";
		postCommentText.style.fontSize = "14px";
		
		var postCommentForm = document.getElementById("postCommentForm");
		postCommentForm.style.backgroundColor = "#D5FFD5";
		
		document.getElementById("postCommentName").innerHTML = "&nbsp;";
		document.getElementById("postCommentButton").innerHTML = "&nbsp;";
	}
	else
	{
		var submitButton = document.getElementById("submitButton");
		submitButton.disabled = false;
		submitButton.value = "Post";
		
		var alertText = document.getElementById("alertText");
		alertText.style.display = "inline";
		
		if (result.charAt(0) === 'p')
			alertText.innerHTML = "There is already a comment pending approval from your IP address.";
		else if (result.charAt(0) === 'b')
			alertText.innerHTML = "Sorry, your IP address has been blocked for commenting.";
		else
			alertText.innerHTML = "Error. Please try again later";
	}
}

update(<?php if ($prjectID !== NULL) echo($prjectID); else echo('null'); ?>);
</script>