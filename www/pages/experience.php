<?php
$languages = Database::GetLanguages();
$numLanguages = count($languages);

$frameworks = Database::GetFrameworks();
$numFrameworks = count($frameworks);

$software = Database::GetSoftware();
$numSoftware = count($software);

echo('<table cellpadding="0" cellspacing="0"><tr><td><h1>Experience</h1></td></tr><tr><td><h4>Languages</h4></td></tr>');

for ($i = 0; $i < $numLanguages; $i++)
	echo('<tr><td style="height:20px"><a name="l' . $languages[$i][0] . '"></a></td></tr><tr><td><h3>' . htmlspecialchars($languages[$i][1]) . '</h3></td></tr><tr><td class="infoText">'  . nl2br($languages[$i][2]) . '</td></tr>');

echo('<tr><td><br /><h4>Frameworks</h4></td></tr>');

for ($i = 0; $i < $numFrameworks; $i++)
	echo('<tr><td style="height:20px"><a name="f' . $frameworks[$i][0] . '"></a></td></tr><tr><td><h3>' . htmlspecialchars($frameworks[$i][1]) . '</h3></td></tr><tr><td class="infoText">'  . nl2br($frameworks[$i][3]) . '<br /><br /><a href="'  . $frameworks[$i][2] . '">'  . htmlspecialchars($frameworks[$i][2]) . '</a></td></tr>');

echo('<tr><td><br /><h4>Software</h4></td></tr>');

for ($i = 0; $i < $numSoftware; $i++)
	echo('<tr><td style="height:20px"><a name="s' . $software[$i][0] . '"></a></td></tr><tr><td><h3>' . htmlspecialchars($software[$i][1]) . '</h3></td></tr><tr><td class="infoText">'  . nl2br($software[$i][3]) . '<br /><br /><a href="'  . $software[$i][2] . '">'  . htmlspecialchars($software[$i][2]) . '</a></td></tr>');

echo('</table>');