<?php
$employers = Database::GetEmployers();
$numEmployers = count($employers);

echo('<table cellpadding="0" cellspacing="0"><tr><td><h1>Employers</h1></td></tr>');

for ($i = 0; $i < $numEmployers; $i++)
	echo('<tr><td style="height:30px"><a name="e' . $employers[$i][0] . '"></a></td></tr><tr><td><h4>' . htmlspecialchars($employers[$i][1]) . ' (' . $employers[$i][2] . ' - ' . ($employers[$i][3] === NULL? 'Present' : $employers[$i][3]) . ')</h4></td></tr><tr><td>' . (strlen($employers[$i][4]) === 0? '<br />' : '<h5>' . htmlspecialchars($employers[$i][4]) . '</h5>') . '</td></tr><tr><td class="infoText">'  . nl2br($employers[$i][6]) . '<br /><br /><a href="' . $employers[$i][5] . '">' . $employers[$i][5] . '</a></td></tr>');

echo('</table>');