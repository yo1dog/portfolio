<?php
include('connection.php');

class Database
{
	//////////////////////////////////////////////////
	// GetProjects
	//
	//  returns projects with the given criteria
	//
	public static function GetProjectsList()
	{
		$query = 'SELECT id, name FROM projects';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$projects = array();
		while ($row = mysql_fetch_row($result))
			array_push($projects, $row);
		
		return $projects;
	}
	
	public static function GetProjects($ordered)
	{
		$query = '
SELECT p.id, p.name, p.year_from, p.year_to, p.category_id, c.name AS category_name, p.employer_id, e.name AS employer_name, p.description, p.link_name, p.link_url, p.image_number
FROM projects AS p
LEFT JOIN employers AS e ON (e.id = p.employer_id)
LEFT JOIN categories AS c ON (c.id = p.category_id)';

		if ($ordered)
			$query .= ' ORDER BY year_from DESC, year_to ASC';
		
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$projects = array();
		while ($row = mysql_fetch_assoc($result))
		{
			$query = 'SELECT pl.language_id, l.name FROM project_languages AS pl LEFT JOIN languages AS l ON (l.id = pl.language_id) WHERE (project_id = ' . $row['id'] . ') ORDER BY l.name ASC';
			$cResult = mysql_query($query);
			 
			if ($cResult === false)
				throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
			
			$row['languages'] = array();
			$cNumRows = mysql_num_rows($cResult);
			for ($i = 0; $i < $cNumRows; $i++)
				$row['languages'][$i] = mysql_fetch_row($cResult);
			
			
			$query = 'SELECT pf.framework_id, f.name FROM project_frameworks AS pf LEFT JOIN frameworks AS f ON (f.id = pf.framework_id) WHERE (project_id = ' . $row['id'] . ') ORDER BY f.name ASC';
			$cResult = mysql_query($query);
			 
			if ($cResult === false)
				throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
			
			$row['frameworks'] = array();
			$cNumRows = mysql_num_rows($cResult);
			for ($i = 0; $i < $cNumRows; $i++)
				$row['frameworks'][$i] = mysql_fetch_row($cResult);
			
			
			$query = 'SELECT ps.software_id, s.name FROM project_software AS ps LEFT JOIN software AS s ON (s.id = ps.software_id) WHERE (project_id = ' . $row['id'] . ') ORDER BY s.name ASC';
			$cResult = mysql_query($query);
			 
			if ($cResult === false)
				throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
			
			$row['software'] = array();
			$cNumRows = mysql_num_rows($cResult);
			for ($i = 0; $i < $cNumRows; $i++)
				$row['software'][$i] = mysql_fetch_row($cResult);
			
			array_push($projects, $row);
		}
		
		return $projects;
	}
	
	//////////////////////////////////////////////////
	// AddProject
	//
	//  adds a new project
	//
	public static function AddProject($name, $yearFrom, $yearTo, $categoryID, $employerID, $description, $linkName, $linkURL, array $languageIDs, array $frameworkIDs, array $softwareIDs)
	{
		if (!GetAdminLoggedIn())
			throw new CustomException(PERMISSION_ERROR, __FILE__, __LINE__);
		
		if ($name === NULL)				$name = 'NULL';				else $name =			'\'' . mysql_real_escape_string($name) . '\'';
		if ($yearFrom === NULL)			$yearFrom = 'NULL';			else $yearFrom =		'\'' . mysql_real_escape_string($yearFrom) . '\'';
		if ($yearTo === NULL)			$yearTo = 'NULL';			else $yearTo =			'\'' . mysql_real_escape_string($yearTo) . '\'';
		if ($categoryID === NULL)		$categoryID = 'NULL';		else $categoryID =		'\'' . mysql_real_escape_string($categoryID) . '\'';
		if ($employerID === NULL)		$employerID = 'NULL';		else $employerID =		'\'' . mysql_real_escape_string($employerID) . '\'';
		if ($description === NULL)		$description = 'NULL';		else $description =		'\'' . mysql_real_escape_string($description) . '\'';
		if ($linkName === NULL)			$linkName = 'NULL';			else $linkName =		'\'' . mysql_real_escape_string($linkName) . '\'';
		if ($linkURL === NULL)			$linkURL = 'NULL';			else $linkURL =			'\'' . mysql_real_escape_string($linkURL) . '\'';
		
		$query = "INSERT INTO projects (name, year_from, year_to, category_id, employer_id, description, link_name, link_url) VALUES ($title, $yearFrom, $yearTo,  $categoryID, $employerID, $description, $linkName, $linkURL)";
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if (mysql_affected_rows() === 0)
			throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		if ($languageIDs !== NULL && $l = count($languageIDs) > 0)
		{
			$id = mysql_insert_id();
			$query = 'INSERT INTO languages (project_id, language_id) VALUES ';
			
			for ($i = 0; $i < $l; $i++)
			{
				if ($i > 0)
					$query . ', ';
				
				$query .= '(' . $id . ', \'' . mysql_real_escape_string($languageIDs[$i]) . '\')';
			}
			
			$result = mysql_query($query);
		
			if ($result === false)
				throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
			
			if (mysql_affected_rows() === 0)
				throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		}
		
		if ($frameworkIDs !== NULL && $l = count($frameworkIDs) > 0)
		{
			$id = mysql_insert_id();
			$query = 'INSERT INTO frameworks (project_id, framework_id) VALUES ';
			
			for ($i = 0; $i < $l; $i++)
			{
				if ($i > 0)
					$query . ', ';
				
				$query .= '(' . $id . ', \'' . mysql_real_escape_string($frameworkIDs[$i]) . '\')';
			}
			
			$result = mysql_query($query);
		
			if ($result === false)
				throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
			
			if (mysql_affected_rows() === 0)
				throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		}
		
		if ($softwareIDs !== NULL && $l = count($softwareIDs) > 0)
		{
			$id = mysql_insert_id();
			$query = 'INSERT INTO software (project_id, software_id) VALUES ';
			
			for ($i = 0; $i < $l; $i++)
			{
				if ($i > 0)
					$query . ', ';
				
				$query .= '(' . $id . ', \'' . mysql_real_escape_string($softwareIDs[$i]) . '\')';
			}
			
			$result = mysql_query($query);
		
			if ($result === false)
				throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
			
			if (mysql_affected_rows() === 0)
				throw new CustomException(MYSQL_NO_ROWS, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		}
		
		return;
	}
	
	//////////////////////////////////////////////////
	// GetProjectComments
	//
	//  returns all the comments for the given project
	//
	public static function GetProjectComments($projectID)
	{
		$query = 'SELECT * FROM project_comments WHERE project_id = \'' . mysql_real_escape_string($projectID) . '\' AND approved = 1 ORDER BY timestamp DESC';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$comments = array();
		while ($row = mysql_fetch_assoc($result))
			array_push($comments, $row);
		
		return $comments;
	}
	
	//////////////////////////////////////////////////
	// GetLanguages
	//
	//  returns all the languages
	//
	public static function GetLanguages()
	{ return Database::_GetLanguages('*'); }
	public static function GetLanguagesList()
	{ return Database::_GetLanguages('id, name'); }
	
	private static function _GetLanguages($selStr)
	{
		$query = 'SELECT ' . $selStr . ' FROM languages ORDER BY name ASC';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$languages = array();
		while ($row = mysql_fetch_row($result))
			array_push($languages, $row);
		
		return $languages;
	}
	
	
	
	//////////////////////////////////////////////////
	// GetFrameworks
	//
	//  returns all the frameworks
	//
	public static function GetFrameworks()
	{ return Database::_GetFrameworks('*'); }
	public static function GetFrameworksList()
	{ return Database::_GetFrameworks('id, name'); }
	
	private static function _GetFrameworks($selStr)
	{
		$query = 'SELECT ' . $selStr . ' FROM frameworks ORDER BY name ASC';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$frameworks = array();
		while ($row = mysql_fetch_row($result))
			array_push($frameworks, $row);
		
		return $frameworks;
	}
	
	//////////////////////////////////////////////////
	// GetSoftware
	//
	//  returns all the software
	//
	public static function GetSoftware()
	{ return Database::_GetSoftware('*'); }
	public static function GetSoftwareList()
	{ return Database::_GetSoftware('id, name'); }
	
	private static function _GetSoftware($selStr)
	{
		$query = 'SELECT ' . $selStr . ' FROM software ORDER BY name ASC';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$software = array();
		while ($row = mysql_fetch_row($result))
			array_push($software, $row);
		
		return $software;
	}
	
	//////////////////////////////////////////////////
	// GetCategories
	//
	//  returns all the categories
	//
	public static function GetCategories()
	{ return Database::_GetCategories('*'); }
	public static function GetCategoriesList()
	{ return Database::_GetCategories('id, name'); }
	
	
	private static function _GetCategories($selStr)
	{
		$query = 'SELECT ' . $selStr . ' FROM categories ORDER BY name ASC';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$categories = array();
		while ($row = mysql_fetch_row($result))
			array_push($categories, $row);
		
		return $categories;
	}
	
	//////////////////////////////////////////////////
	// GetEmployers
	//
	//  returns all the employers
	//
	public static function GetEmployers()
	{ return Database::_GetEmployers('*'); }
	public static function GetEmployersList()
	{ return Database::_GetEmployers('id, name'); }
	
	public static function _GetEmployers($selStr)
	{
		$query = 'SELECT ' . $selStr . ' FROM employers ORDER BY year_to DESC, year_from DESC';
		$result = mysql_query($query);
		
		if ($result === false)
			throw new CustomException(MYSQL_QUERY_ERROR, __FILE__, __LINE__, mysql_error(), mysql_errno(), $query);
		
		$employers = array();
		while ($row = mysql_fetch_row($result))
			array_push($employers, $row);
		
		return $employers;
	}
}
?>