<?php
define('PAGE_WIDTH', 960);
?>

<html>
<head>
<title>Mike Moore</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<meta NAME="ROBOTS" CONTENT="All">
<link REL="styleSheet" HREF="style.css" TYPE="text/css">
<link REL="styleSheet" HREF="styleShared.css" TYPE="text/css">
<link REL="SHORTCUT ICON" HREF="/images/favicon4.ico">

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10797534-4', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics -->

</head>
<body>
<table cellpadding="0" cellspacing="0" class="bodyTable">
	<tr>
		<td class="bodyLeft">
        </td>
        <td class="body">
        	<div style="height:1px"><img src="images/spacer.png" width="<?php echo(PAGE_WIDTH); ?>" height="1"/></div>
            
        	<div class="headerTop">Mike Moore</div>
            
        	<table class="tabs" cellpadding="0" cellspacing="0">
            	<tr id="tabsRow">
                	<?php
					$previousSelected = false;
                    if ($page === NULL || $page === 'home')
					{
						echo('<td class="tabSelected">Home</td>');
						$previousSelected = true;
					}
					else
						echo('<td class="tabB"><a href="?p=home">Home</a></td>');
					
					if ($page === 'experience')
					{
						$previousSelected = true;
						echo('<td class="tabSelected">Experience</td>');
					}
					else
					{
						echo('<td class="' . ($previousSelected? 'tabB' : 'tab') . '"><a href="?p=experience">Experience</a></td>');
						$previousSelected = false;
					}
					
					if ($page === 'projects')
					{
						$previousSelected = true;
						echo('<td class="tabSelected">Projects</td>');
					}
					else
					{
						echo('<td class="' . ($previousSelected? 'tabB' : 'tab') . '"><a href="?p=projects">Projects</a></td>');
						$previousSelected = false;
					}
					
					if ($page === 'employers')
					{
						$previousSelected = true;
						echo('<td class="tabSelected">Employers</td>');
					}
					else
					{
						echo('<td class="' . ($previousSelected? 'tabB' : 'tab') . '"><a href="?p=employers">Employers</a></td>');
						$previousSelected = false;
					}
					
					if ($page === 'contact')
					{
						$previousSelected = true;
						echo('<td class="tabSelected">Contact</td>');
					}
					else
					{
						echo('<td class="' . ($previousSelected? 'tabB' : 'tab') . '"><a href="?p=contact">Contact</a></td>');
						$previousSelected = false;
					}
					?>
                    
                </tr>
            </table>
            
            <table style="width:100%" cellpadding="0" cellspacing="0">
            	<tr>
                	<td class="bodyContent">
                <!--<script type="text/javascript" language="javascript">
                function tabOver(id)
                {
                    document.getElementById("tab" + id).className = "tabOver";
                }
                
                function tabOut(id)
                {
                    document.getElementById("tab" + id).className = "tab";
                }
                
                function tabClick(url)
                {
                    window.location = url;
                }
                
                function addTab(title, url)
                {
                    var tabsRow = document.getElementById("tabsRow");
                    var id = tabsRow.cells.length;
                    var cell = tabsRow.insertCell(id);
                    cell.setAttribute("id", "tab" + id);
                    cell.setAttribute("onmouseover", "tabOver(" + id + ")");
                    cell.setAttribute("onmouseout", "tabOut(" + id + ")");
                    cell.setAttribute("onclick", "tabClick('" + url + "')");
                    cell.setAttribute("class", "tab");
                    cell.innerHTML = title;
                }
			</script>-->
            
            	
